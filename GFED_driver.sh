#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# description
# ----------------------------------------------------------------------

# A top-level driver for processing annual GFED data from global/native to a form suitable for a CMAQ run over the AQMEII-NA domain. It

# * downloads native data from GFED site
# * converts ASCII data to netCDF
# * 2D-regrids from global/unprojected to AQMEII-NA/LCC
# * retemporalizes from monthly to hourly
# * outputs in form consumable by CMAQ (hopefully)
# * ... with visualizations and conservation checks

# See https://bitbucket.org/epa_n2o_project_team/gfed-3.1_global_to_aqmeii-na

# Expects to run on linux. Utility dependencies include:

# * definitely

# ** bash: required to run this script. Known to work with bash --version==3.2.25

# * potentially initially. If you have not already retrieved regrid_utils (see `get_regrid_utils`), you will need

# ** git: to clone their repo
# ** web access (HTTP currently)

#   These must be available to the script when initially run.

# * ultimately

# ** basename, dirname
# ** cp
# ** curl or wget (latter {preferred, coded} for ability to deal with redirects)
# ** gunzip, unzip
# ** ncdump
# ** NCL
# ** R

# TODO: failing functions should fail this (entire) driver!

# Configure as needed for your platform.

# ----------------------------------------------------------------------
# constants with some simple manipulations
# ----------------------------------------------------------------------

# TODO: take switches for help, debugging, no/eval, target drive
THIS="$0"
THIS_FN="$(basename $0)"
THIS_DIR="$(dirname $0)"

### workspace
# note: following will fail if `source`ing!
export WORK_DIR="${THIS_DIR}" # where we expect to find stuff

### downloading
WGET_TO_FILE='wget --no-check-certificate -c -O'
WGET_TO_STREAM="${WGET_TO_FILE} -"
CURL_TO_FILE='curl -C - -o'
CURL_TO_STREAM='curl'

### model/inventory constants
export MODEL_YEAR='2008'
export MONTHS_PER_YEAR='12'
export HOURS_PER_DAY='24'
export SECONDS_PER_HOUR='3600'
export HOURS_PER_SECOND='2.778e-4' # 1/3600
# GFED can't be consistent?
GFED_MONTHLY_VERSION='GFED3.1'
GFED_FRACTION_VERSION='GFEDv3.1'
# ... us either :-)
GFED_OUTPUT_VERSION='GFED-3.1'
# where this is hosted
PROJECT_WEBSITE='https://bitbucket.org/epa_n2o_project_team/gfed-3.1_global_to_aqmeii-na'
NETCDF_EXT_NCL='nc'   # NCL prefers the "canonical" netCDF extension
NETCDF_EXT_CMAQ='ncf' # CMAQ, as is so often the case, is nonstandard :-(
## regrid constants
export GFED_REGRID_N_ROW='299'
export GFED_REGRID_N_COL='459'
# IOAPI metadata: see http://www.baronams.com/products/ioapi/INCLUDE.html#fdesc
export IOAPI_TIMESTEP_LENGTH='10000' # :TSTEP
export IOAPI_START_OF_TIMESTEPS='0'  # :STIME

### numerical and math constants
export ZERO_MAX='0.0001' # any value less than this is probably 0
export ONE_MIN='0.9999'  # any value more than this is probably 1
export CMAQ_RADIUS='6370000' # of spherical earth, in meters
# pi, etc

### empirical constants
export MOLAR_MASS_N2O='44.0128' # grams per mole of N2O, per wolframalpha.com

### AQMEII-NA constants

## datavar attributes
# TODO: get from template file or from EPIC
# "real" data
AQMEIINA_DV_NAME='N2O'
# IOAPI pads varattr=long_name to length=16 with trailing spaces
AQMEIINA_DV_LONG_NAME="$(printf '%-16s' ${AQMEIINA_DV_NAME})"
# IOAPI pads varattr=units to length=16 with trailing spaces
AQMEIINA_DV_UNITS="$(printf '%-16s' 'moles/s')"
# IOAPI pads varattr=var_desc to length=80 with trailing spaces
# Don't single-quote the payload: double-quote it (OK inside parens inside double-quotes)
AQMEIINA_DV_VAR_DESC="$(printf '%-80s' "Model species ${AQMEIINA_DV_NAME}")"

## "fake" datavar=TFLAG, required by IOAPI (and more to the point (IIUC), VERDI)
AQMEIINA_TFLAG_NAME='TFLAG'
# IOAPI pads varattr=long_name to length=16 with trailing spaces
AQMEIINA_TFLAG_LONG_NAME="$(printf '%-16s' ${AQMEIINA_TFLAG_NAME})"
# IOAPI pads varattr=units to length=16 with trailing spaces
AQMEIINA_TFLAG_UNITS="$(printf '%-16s' '<YYYYDDD,HHMMSS>')"
# IOAPI pads varattr=var_desc to length=80 with trailing spaces
AQMEIINA_TFLAG_VAR_DESC="$(printf '%-80s' 'Timestep-valid flags:  (1) YYYYDDD or (2) HHMMSS')"

## dimensions and their attributes
AQMEIINA_DIM_LAYER_NAME='LAY'
AQMEIINA_DIM_LAYER_LONG_NAME='index of layers above surface'
AQMEIINA_DIM_LAYER_UNITS='unitless'

AQMEIINA_DIM_TSTEP_NAME='TSTEP'
AQMEIINA_DIM_TSTEP_UNITS='' # they vary!
AQMEIINA_DIM_TSTEP_LONG_NAME='timestep'

# dimensions we don't really need, but VERDI/IOAPI/TFLAG does
AQMEIINA_DIM_DATETIME_NAME='DATE-TIME'
AQMEIINA_DIM_VAR_NAME='VAR'

AQMEIINA_DIM_X_N='459'
AQMEIINA_DIM_X_NAME='COL'
# AQMEIINA_DIM_X_UNITS='unitless' # my invention
AQMEIINA_DIM_X_UNITS='m'
# AQMEIINA_DIM_X_LONG_NAME='Fortran-style index to grid columns from lower-left origin' # my invention (TODO: CHECK it's not from top)
AQMEIINA_DIM_X_LONG_NAME='grid-center offset from center of projection'

AQMEIINA_DIM_Y_N='299'
AQMEIINA_DIM_Y_NAME='ROW'
# AQMEIINA_DIM_Y_UNITS='unitless' # my invention
AQMEIINA_DIM_Y_UNITS='m'
# AQMEIINA_DIM_Y_LONG_NAME='Fortran-style index to grid columns from lower-left origin' # my invention (TODO: CHECK it's not from top)
AQMEIINA_DIM_Y_LONG_NAME="${AQMEIINA_DIM_X_LONG_NAME}"

### artifact visualization

## for visualization (generally)
export OUTPUT_SIGNIFICANT_DIGITS='3'
# PROJ.4 string for unprojected data
export GLOBAL_PROJ4='+proj=longlat +ellps=WGS84'

## for plotting (specifically)
# PDF_VIEWER='xpdf' # set this in bash_utilities::setup_apps
# temporally disaggregate multiple plots
DATE_FORMAT='%Y%m%d_%H%M'
export PDF_DIR="${WORK_DIR}"
# dimensions (for R plots--units?) for single-frame plots
export SINGLE_FRAME_PLOT_HEIGHT='10'
export SINGLE_FRAME_PLOT_WIDTH='15'
# dimensions for multi-frame plots
export TWELVE_MONTH_PLOT_HEIGHT='120'
export TWELVE_MONTH_PLOT_WIDTH='20'

## conservation report constants
# need 5 additional digits for float output, e.g., "4.85e+04"
# bash arithmetic gotcha: allow no spaces around '='!
# export GFED_CONSERV_REPORT_FIELD_WIDTH=$((OUTPUT_SIGNIFICANT_DIGITS + 5))
export GFED_CONSERV_REPORT_FIELD_WIDTH='9' # width of 'AQMEII-NA'
export GFED_CONSERV_REPORT_COLUMN_SEPARATOR="  "
export GFED_CONSERV_REPORT_TITLE="Is N2O conserved from input to output? units=mol N2O"
export GFED_CONSERV_REPORT_SUBTITLE="(note (US land area)/(earth land area) ~= 6.15e-02)"
export GFED_CONSERV_REPORT_FLOAT_FORMAT="%${GFED_CONSERV_REPORT_FIELD_WIDTH}.$((OUTPUT_SIGNIFICANT_DIGITS - 1))e"
# echo -e "${THIS_FN}: GFED_CONSERV_REPORT_FLOAT_FORMAT=${GFED_CONSERV_REPORT_FLOAT_FORMAT}" # debugging
export GFED_CONSERV_REPORT_INT_FORMAT="%${GFED_CONSERV_REPORT_FIELD_WIDTH}i"

# ----------------------------------------------------------------------
# helpers
# ----------------------------------------------------------------------

### helpers in this repo

export CHECK_COORD_VARS_FN='check_coord_vars.ncl'
export CHECK_COORD_VARS_FP="${WORK_DIR}/${CHECK_COORD_VARS_FN}"

export CHECK_CONSERVATION_FN='check_conservation.ncl'
export CHECK_CONSERVATION_FP="${WORK_DIR}/${CHECK_CONSERVATION_FN}"

export CHECK_RAW_FRACS_FN='check_raw_fractions.ncl'
export CHECK_RAW_FRACS_FP="${WORK_DIR}/${CHECK_RAW_FRACS_FN}"

export CONV_DAILIES_FN='convert_dailies.ncl'
export CONV_DAILIES_FP="${WORK_DIR}/${CONV_DAILIES_FN}"

export CONV_MONTHLIES_FN='convert_monthlies.ncl'
export CONV_MONTHLIES_FP="${WORK_DIR}/${CONV_MONTHLIES_FN}"

export CONV_3HOURLIES_FN='convert_3hourlies.ncl'
export CONV_3HOURLIES_FP="${WORK_DIR}/${CONV_3HOURLIES_FN}"

export FIX_3HOURLIES_FN='fix_3hourlies.ncl'
export FIX_3HOURLIES_FP="${WORK_DIR}/${FIX_3HOURLIES_FN}"

export MAKE_HOURLIES_FN='make_hourlies.ncl'
export MAKE_HOURLIES_FP="${WORK_DIR}/${MAKE_HOURLIES_FN}"

export VIS_REGRID_VIS_FN='vis_regrid_vis.r'
export VIS_REGRID_VIS_FP="${WORK_DIR}/${VIS_REGRID_VIS_FN}"

### helpers retrieved from elsewhere. TODO: create NCL and R packages

# path to a project, not a .git
REGRID_UTILS_URI='https://bitbucket.org/epa_n2o_project_team/regrid_utils'
REGRID_UTILS_PN="$(basename ${REGRID_UTILS_URI})" # project name
# can also use   'git@bitbucket.org:tlroche/regrid_utils' if supported
# path to a folder, not a file: needed by NCL to get to initial helpers
export REGRID_UTILS_DIR="${WORK_DIR}/${REGRID_UTILS_PN}" # folder, not file

export BASH_UTILS_FN='bash_utilities.sh'
# export BASH_UTILS_FP="${REGRID_UTILS_DIR}/${BASH_UTILS_FN}"
# no, allow user to override repo code: see `get_bash_utils`
export BASH_UTILS_FP="${WORK_DIR}/${BASH_UTILS_FN}"

export FILEPATH_FUNCS_FN='get_filepath_from_template.ncl'
export FILEPATH_FUNCS_FP="${WORK_DIR}/${FILEPATH_FUNCS_FN}"

export GET_INPUT_AREAS_FN='get_input_areas.ncl'
export GET_INPUT_AREAS_FP="${WORK_DIR}/${GET_INPUT_AREAS_FN}"

export GET_OUTPUT_AREAS_FN='get_output_areas.ncl'
export GET_OUTPUT_AREAS_FP="${WORK_DIR}/${GET_OUTPUT_AREAS_FN}"

export IOAPI_FUNCS_FN='IOAPI.ncl'
export IOAPI_FUNCS_FP="${WORK_DIR}/${IOAPI_FUNCS_FN}"

export STATS_FUNCS_FN='netCDF.stats.to.stdout.r'
export STATS_FUNCS_FP="${WORK_DIR}/${STATS_FUNCS_FN}"

export STRING_FUNCS_FN='string.ncl'
export STRING_FUNCS_FP="${WORK_DIR}/${STRING_FUNCS_FN}"

export SUMMARIZE_FUNCS_FN='summarize.ncl'
export SUMMARIZE_FUNCS_FP="${WORK_DIR}/${SUMMARIZE_FUNCS_FN}"

export TIME_FUNCS_FN='time.ncl'
export TIME_FUNCS_FP="${WORK_DIR}/${TIME_FUNCS_FN}"

export VIS_FUNCS_FN='visualization.r'
export VIS_FUNCS_FP="${WORK_DIR}/${VIS_FUNCS_FN}"

# ----------------------------------------------------------------------
# raw inputs
# ----------------------------------------------------------------------

# this template character ('@') supports string substitution in both bash and NCL
export TEMPLATE_STRING='@@@@@@@@' # for all replacements
export JANUARY_STRING="${MODEL_YEAR}01"
export JANUARY_FIRST_STRING="${JANUARY_STRING}01"

### GFED downloads
## monthly emissions
GFED_GLOBAL_MONTHLY_ZIP_URI="http://www.falw.vu/~gwerf/GFED/GFED3/emissions/${GFED_MONTHLY_VERSION}_N2O.zip"
GFED_GLOBAL_MONTHLY_ZIP_FN="$(basename ${GFED_GLOBAL_MONTHLY_ZIP_URI%%\?*})"
GFED_GLOBAL_MONTHLY_ZIP_FP="${WORK_DIR}/${GFED_GLOBAL_MONTHLY_ZIP_FN}"
# this sort of template ('?') works from commandline
GFED_GLOBAL_MONTHLY_TEMPLATE="${GFED_MONTHLY_VERSION}_${MODEL_YEAR}??_N2O.txt"
## These files each contain one month's total emissions
# export GFED_GLOBAL_MONTHLY_TEMPLATE_STRING='@@'
GFED_GLOBAL_MONTHLY_FN_TEMPLATE="${GFED_MONTHLY_VERSION}_${TEMPLATE_STRING}_N2O.txt"
export GFED_GLOBAL_MONTHLY_FP_TEMPLATE="${WORK_DIR}/${GFED_GLOBAL_MONTHLY_FN_TEMPLATE}"

# .txt file schema:
# * no headers or footers
export GFED_GLOBAL_MONTHLY_TXT_HEADERS='0'
export GFED_GLOBAL_MONTHLY_TXT_FOOTERS='0'
# * 360 rows (per http://www.globalfiredata.org/Data/ and `wc -l`)
export GFED_GLOBAL_MONTHLY_TXT_ROWS='360'
# * 720 rows (per http://www.globalfiredata.org/Data/ )
export GFED_GLOBAL_MONTHLY_TXT_COLS='720'
# just the emissions for January
GFED_GLOBAL_MONTHLY_JAN_FN="${GFED_GLOBAL_MONTHLY_FN_TEMPLATE/${TEMPLATE_STRING}/${JANUARY_STRING}}"
GFED_GLOBAL_MONTHLY_JAN_FP="${WORK_DIR}/${GFED_GLOBAL_MONTHLY_JAN_FN}"

## fractions README
export GFED_FRACS_README_URI="ftp://gfed3:dailyandhourly@zea.ess.uci.edu/${GFED_FRACTION_VERSION}/Readme.pdf"

## daily fractions
# workaround for when zea.ess.uci.edu is down
GFED_GLOBAL_DAILY_FRAC_GZ_FN="${MODEL_YEAR}.tar.gz"
GFED_GLOBAL_DAILY_FRAC_GZ_FAKE_FN="daily.${GFED_GLOBAL_DAILY_FRAC_GZ_FN}"
GFED_GLOBAL_DAILY_FRAC_GZ_FAKE_FP="${WORK_DIR}/${GFED_GLOBAL_DAILY_FRAC_GZ_FAKE_FN}"
GFED_GLOBAL_DAILY_FRAC_GZ_FAKE_URI=".../${GFED_GLOBAL_DAILY_FRAC_GZ_FN}" # TODO: cache on bitbucket
GFED_GLOBAL_DAILY_FRAC_GZ_URI="ftp://gfed3:dailyandhourly@zea.ess.uci.edu/${GFED_FRACTION_VERSION}/daily/${GFED_GLOBAL_DAILY_FRAC_GZ_FN}"
## These files each contain one day's fractional emissions
GFED_GLOBAL_DAILY_FRAC_TEMPLATE="fraction_emissions_${MODEL_YEAR}????.${NETCDF_EXT_NCL}"    # for commandline
# export GFED_GLOBAL_DAILY_FRAC_TEMPLATE_STRING='@@@@'
GFED_GLOBAL_DAILY_FRAC_FN_TEMPLATE="fraction_emissions_${TEMPLATE_STRING}.${NETCDF_EXT_NCL}"
export GFED_GLOBAL_DAILY_FRAC_FP_TEMPLATE="${WORK_DIR}/${GFED_GLOBAL_DAILY_FRAC_FN_TEMPLATE}"
export GFED_GLOBAL_DAILY_FRAC_DATAVAR_NAME='Fraction_of_Emissions'
# just the daily fractions for 1 Jan
GFED_GLOBAL_DAILY_FRAC_JAN_01_FN="${GFED_GLOBAL_DAILY_FRAC_FN_TEMPLATE/${TEMPLATE_STRING}/${JANUARY_FIRST_STRING}}"
export GFED_GLOBAL_DAILY_FRAC_JAN_01_FP="${WORK_DIR}/${GFED_GLOBAL_DAILY_FRAC_JAN_01_FN}"
# echo -e "GFED_GLOBAL_DAILY_FRAC_JAN_01_FP=${GFED_GLOBAL_DAILY_FRAC_JAN_01_FP}" # debugging

## 3-hour fractions
# workaround for when zea.ess.uci.edu is down
GFED_GLOBAL_3HOURLY_FRAC_GZ_FN="${MODEL_YEAR}.tar.gz"
GFED_GLOBAL_3HOURLY_FRAC_GZ_FAKE_FN="3hourly.${GFED_GLOBAL_3HOURLY_FRAC_GZ_FN}"
GFED_GLOBAL_3HOURLY_FRAC_GZ_FAKE_FP="${WORK_DIR}/${GFED_GLOBAL_3HOURLY_FRAC_GZ_FAKE_FN}"
GFED_GLOBAL_3HOURLY_FRAC_GZ_FAKE_URI=".../${GFED_GLOBAL_3HOURLY_FRAC_GZ_FN}" # TODO: cache on bitbucket
GFED_GLOBAL_3HOURLY_FRAC_GZ_URI="ftp://gfed3:dailyandhourly@zea.ess.uci.edu/${GFED_FRACTION_VERSION}/3-hourly/${GFED_GLOBAL_3HOURLY_FRAC_GZ_FN}"
## These files each contain one month's fractional emissions for each 3-hour period.
GFED_GLOBAL_3HOURLY_FRAC_TEMPLATE="fraction_emissions_${MODEL_YEAR}??.${NETCDF_EXT_NCL}"
# export GFED_GLOBAL_3HOURLY_FRAC_TEMPLATE_STRING='@@'
GFED_GLOBAL_3HOURLY_FRAC_FN_TEMPLATE="fraction_emissions_${TEMPLATE_STRING}.${NETCDF_EXT_NCL}"
export GFED_GLOBAL_3HOURLY_FRAC_FP_TEMPLATE="${WORK_DIR}/${GFED_GLOBAL_3HOURLY_FRAC_FN_TEMPLATE}"
export GFED_GLOBAL_3HOURLY_FRAC_DATAVAR_NAME='Fraction_of_Emissions'
export GFED_GLOBAL_3HOURLY_FRAC_TIME_DIM_POSN='0' # C-style position of time dimension in fraction datavar
# just the 3-hour fractions for January
GFED_GLOBAL_3HOURLY_FRAC_JAN_FN="${GFED_GLOBAL_3HOURLY_FRAC_FN_TEMPLATE/${TEMPLATE_STRING}/${JANUARY_STRING}}"
export GFED_GLOBAL_3HOURLY_FRAC_JAN_FP="${WORK_DIR}/${GFED_GLOBAL_3HOURLY_FRAC_JAN_FN}"

### Template for `raster` regridding and IOAPI-writing.

# This file should have all required IOAPI metadata (including the "fake datavar"=TFLAG) for a single datavar=N2O,
# but also the correct extent, grid, and other spatial parameters for our domain, to use in `raster` regridding.
TEMPLATE_INPUT_GZ_URI='https://bitbucket.org/epa_n2o_project_team/aqmeii_ag_soil/downloads/emis_mole_N2O_2008_12US1_cmaq_cb05_soa_2008ab_08c.ncf.gz' # final yearly output from AQMEII_ag_soil, VERDI-viewable
# formerly also used
# TEMPLATE_EXTENT_GZ_URI='https://bitbucket.org/epa_n2o_project_team/geia_regrid/downloads/emis_mole_all_20080101_12US1_cmaq_cb05_soa_2008ab_08c.EXTENTS_INPUT.nc.gz'
# et al
TEMPLATE_INPUT_GZ_FN="$(basename ${TEMPLATE_INPUT_GZ_URI})"
TEMPLATE_INPUT_GZ_FP="${WORK_DIR}/${TEMPLATE_INPUT_GZ_FN}"
# TEMPLATE_INPUT_ROOT="${TEMPLATE_INPUT_GZ_FN%.*}" # everything left of the LAST dot
# TEMPLATE_INPUT_FN="${TEMPLATE_INPUT_ROOT}"
# that will collide with this project's final output! so instead
TEMPLATE_INPUT_FN='aqmeii_ag_soil_yearly.nc'
export TEMPLATE_INPUT_FP="${WORK_DIR}/${TEMPLATE_INPUT_FN}"
export TEMPLATE_INPUT_DATAVAR_NAME="${AQMEIINA_DV_NAME}"
export TEMPLATE_INPUT_TFLAG_NAME="${AQMEIINA_TFLAG_NAME}"
export TEMPLATE_INPUT_BAND='1' # since position of TSTEP in N2O(TSTEP, LAY, ROW, COL)? seems to work

# ----------------------------------------------------------------------
# intermediate products
# ----------------------------------------------------------------------

### intermediate product: GFED global gridcell areas
## ASSERT: all GFED grids have same resolution (0.5°x0.5°, ROWxCOL) and definitions (grid centers and edges).
## Therefore, can get gridcell areas from any sample file.
export GFED_AREAS_GLOBAL_SAMPLE_FP="${GFED_GLOBAL_DAILY_FRAC_JAN_01_FP}"
export GFED_AREAS_GLOBAL_SAMPLE_DATAVAR_NAME="${GFED_GLOBAL_DAILY_FRAC_DATAVAR_NAME}"
export GFED_AREAS_GLOBAL_SAMPLE_DIM_ROW_NAME='lat'
export GFED_AREAS_GLOBAL_SAMPLE_DIM_COL_NAME='lon'

GFED_AREAS_GLOBAL_FN="areas_0.5x0.5.${NETCDF_EXT_NCL}" # like EDGAR (but different resolution)
export GFED_AREAS_GLOBAL_FP="${WORK_DIR}/${GFED_AREAS_GLOBAL_FN}" # must not exist!
export GFED_AREAS_GLOBAL_DATAVAR_NAME='cell_area'              # like EDGAR
export GFED_AREAS_GLOBAL_DATAVAR_LONG_NAME='area of grid cell' # like EDGAR
export GFED_AREAS_GLOBAL_DATAVAR_STANDARD_NAME='area'          # like EDGAR
export GFED_AREAS_GLOBAL_DATAVAR_UNITS='m2'                    # like EDGAR
export GFED_AREAS_GLOBAL_DIM_ROW_NAME='ROW' # long_name, units like input
export GFED_AREAS_GLOBAL_DIM_COL_NAME='COL' # long_name, units like input
export GFED_AREAS_GLOBAL_HISTORY="see ${PROJECT_WEBSITE}"

### intermediate product: output/AQMEII-NA gridcell areas
export AQMEIINA_NOMINAL_GRIDCELL_AREA='1.440e8' # for 12-km grid, in meter^2; make float

## gridcell map scale factors (MSFs) from MCIP for CDC PHASE run
AQMEIINA_MSFs_GZ_URI='https://bitbucket.org/epa_n2o_project_team/clm_cn_global_to_aqmeii-na/downloads/GRIDCRO2D_080101.gz'
AQMEIINA_MSFs_GZ_FN="$(basename ${AQMEIINA_MSFs_GZ_URI})"
AQMEIINA_MSFs_FN_ROOT="${AQMEIINA_MSFs_GZ_FN%.*}" # everything left of the dot
AQMEIINA_MSFs_FN="${AQMEIINA_MSFs_FN_ROOT}"       # does not normally have an extension
AQMEIINA_MSFs_FN_NCL="${AQMEIINA_MSFs_FN}.${NETCDF_EXT_NCL}" # ... which makes NCL vomit
AQMEIINA_MSFs_GZ_FP="${WORK_DIR}/${AQMEIINA_MSFs_GZ_FN}"
export AQMEIINA_MSFs_FP="${WORK_DIR}/${AQMEIINA_MSFs_FN}"
export AQMEIINA_MSFs_FP_NCL="${WORK_DIR}/${AQMEIINA_MSFs_FN_NCL}"
export AQMEIINA_MSFs_DATAVAR_NAME='MSFX2'
export AQMEIINA_MSFs_DIM_ROW_NAME='ROW' # long_name, units like input
export AQMEIINA_MSFs_DIM_COL_NAME='COL' # long_name, units like input

## output areas file
AQMEIINA_AREAS_FN="areas_AQMEII-NA.${NETCDF_EXT_NCL}"
export AQMEIINA_AREAS_FP="${WORK_DIR}/${AQMEIINA_AREAS_FN}" # must not exist @ write-time
export AQMEIINA_AREAS_DATAVAR_NAME='cell_area'              # like EDGAR
export AQMEIINA_AREAS_DATAVAR_LONG_NAME='area of grid cell' # like EDGAR
export AQMEIINA_AREAS_DATAVAR_STANDARD_NAME='area'          # like EDGAR
export AQMEIINA_AREAS_DATAVAR_UNITS='m2'                    # like EDGAR
export AQMEIINA_AREAS_DIM_ROW_NAME='ROW' # long_name, units like input
export AQMEIINA_AREAS_DIM_COL_NAME='COL' # long_name, units like input
export AQMEIINA_AREAS_HISTORY="see ${PROJECT_WEBSITE}"

### intermediate product: netCDFed aggregation of molarized GFED global monthly emissions
GFED_GLOBAL_MONTHLY_CONV_FN_ROOT="${GFED_OUTPUT_VERSION}_${MODEL_YEAR}_N2O_monthly_emissions"
GFED_GLOBAL_MONTHLY_CONV_FN="${GFED_GLOBAL_MONTHLY_CONV_FN_ROOT}.${NETCDF_EXT_NCL}"
export GFED_GLOBAL_MONTHLY_CONV_FP="${WORK_DIR}/${GFED_GLOBAL_MONTHLY_CONV_FN}"
## file/global attributes
export GFED_GLOBAL_MONTHLY_CONV_HISTORY="see ${PROJECT_WEBSITE}"
export GFED_GLOBAL_MONTHLY_CONV_SOURCE_FILE="assembled from data from ${GFED_GLOBAL_MONTHLY_ZIP_URI}"
export GFED_GLOBAL_MONTHLY_CONV_TITLE="monthly GFED N2O emissions for ${MODEL_YEAR}"

## datavar
export GFED_GLOBAL_MONTHLY_CONV_DATAVAR_NAME='N2O'
export GFED_GLOBAL_MONTHLY_CONV_DATAVAR_UNITS='molN20/mo'
export GFED_GLOBAL_MONTHLY_CONV_DATAVAR_LONG_NAME='N2O emissions'
# datavar dims=(TSTEP, LAY, ROW, COL)
export GFED_GLOBAL_MONTHLY_CONV_N_TSTEP="${MONTHS_PER_YEAR}"
export GFED_GLOBAL_MONTHLY_CONV_N_LAYER='1' # one species in file
# still global -> same rows, cols as input .txt
export GFED_GLOBAL_MONTHLY_CONV_N_ROW="${GFED_GLOBAL_MONTHLY_TXT_ROWS}"
export GFED_GLOBAL_MONTHLY_CONV_N_COL="${GFED_GLOBAL_MONTHLY_TXT_COLS}"

# datavar dims and attributes
export GFED_GLOBAL_MONTHLY_CONV_DIM_TIME_NAME='TSTEP'
export GFED_GLOBAL_MONTHLY_CONV_DIM_TIME_UNITS='month'
export GFED_GLOBAL_MONTHLY_CONV_DIM_TIME_LONG_NAME="index of month in ${MODEL_YEAR}"
export GFED_GLOBAL_MONTHLY_CONV_DIM_LAYER_NAME='LAY'
export GFED_GLOBAL_MONTHLY_CONV_DIM_LAYER_LONG_NAME='index of vertical layers'
export GFED_GLOBAL_MONTHLY_CONV_DIM_LAYER_UNITS='unitless'
export GFED_GLOBAL_MONTHLY_CONV_DIM_X_NAME='COL'
export GFED_GLOBAL_MONTHLY_CONV_DIM_X_UNITS='longitude'
export GFED_GLOBAL_MONTHLY_CONV_DIM_X_LONG_NAME='degrees_east'
export GFED_GLOBAL_MONTHLY_CONV_DIM_Y_NAME='ROW'
export GFED_GLOBAL_MONTHLY_CONV_DIM_Y_UNITS='degrees_north'
export GFED_GLOBAL_MONTHLY_CONV_DIM_Y_LONG_NAME='latitude'

### intermediate product: aggregation of GFED global daily fractions
GFED_GLOBAL_DAILY_FRAC_CONV_FN_ROOT="${GFED_OUTPUT_VERSION}_${MODEL_YEAR}_N2O_daily_fractions"
GFED_GLOBAL_DAILY_FRAC_CONV_FN="${GFED_GLOBAL_DAILY_FRAC_CONV_FN_ROOT}.${NETCDF_EXT_NCL}"
export GFED_GLOBAL_DAILY_FRAC_CONV_FP="${WORK_DIR}/${GFED_GLOBAL_DAILY_FRAC_CONV_FN}"
## file/global attributes
export GFED_GLOBAL_DAILY_FRAC_CONV_HISTORY="see ${PROJECT_WEBSITE}"
export GFED_GLOBAL_DAILY_FRAC_CONV_SOURCE_FILE="assembled from data from ${GFED_GLOBAL_DAILY_FRAC_GZ_URI}"
export GFED_GLOBAL_DAILY_FRAC_CONV_TITLE="daily GFED N2O fractions for ${MODEL_YEAR}"

## datavar
export GFED_GLOBAL_DAILY_FRAC_CONV_DATAVAR_NAME="${GFED_GLOBAL_DAILY_FRAC_DATAVAR_NAME}"
export GFED_GLOBAL_DAILY_FRAC_CONV_DATAVAR_UNITS='unitless'
export GFED_GLOBAL_DAILY_FRAC_CONV_DATAVAR_LONG_NAME='daily fraction of monthly N2O emission'
# datavar dims=(TSTEP, LAY, ROW, COL)
# export GFED_GLOBAL_DAILY_FRAC_CONV_N_TSTEP="${DAYS_PER_YEAR}" # calculate
export GFED_GLOBAL_DAILY_FRAC_CONV_N_LAYER='1' # one species in file
# still global -> same rows, cols as input .txt
export GFED_GLOBAL_DAILY_FRAC_CONV_N_ROW="${GFED_GLOBAL_MONTHLY_TXT_ROWS}"
export GFED_GLOBAL_DAILY_FRAC_CONV_N_COL="${GFED_GLOBAL_MONTHLY_TXT_COLS}"

# datavar dims and attributes
export GFED_GLOBAL_DAILY_FRAC_CONV_DIM_TIME_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_TIME_NAME}"
export GFED_GLOBAL_DAILY_FRAC_CONV_DIM_TIME_UNITS='day'
export GFED_GLOBAL_DAILY_FRAC_CONV_DIM_TIME_LONG_NAME="index of day in ${MODEL_YEAR}"
export GFED_GLOBAL_DAILY_FRAC_CONV_DIM_LAYER_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_LAYER_NAME}"
export GFED_GLOBAL_DAILY_FRAC_CONV_DIM_LAYER_LONG_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_LAYER_LONG_NAME}"
export GFED_GLOBAL_DAILY_FRAC_CONV_DIM_LAYER_UNITS="${GFED_GLOBAL_MONTHLY_CONV_DIM_LAYER_UNITS}"
export GFED_GLOBAL_DAILY_FRAC_CONV_DIM_X_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_X_NAME}"
export GFED_GLOBAL_DAILY_FRAC_CONV_DIM_X_UNITS="${GFED_GLOBAL_MONTHLY_CONV_DIM_X_UNITS}"
export GFED_GLOBAL_DAILY_FRAC_CONV_DIM_X_LONG_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_X_LONG_NAME}"
export GFED_GLOBAL_DAILY_FRAC_CONV_DIM_Y_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_Y_NAME}"
export GFED_GLOBAL_DAILY_FRAC_CONV_DIM_Y_UNITS="${GFED_GLOBAL_MONTHLY_CONV_DIM_Y_UNITS}"
export GFED_GLOBAL_DAILY_FRAC_CONV_DIM_Y_LONG_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_Y_LONG_NAME}"

### intermediate product: aggregation of GFED global 3hourly fractions
GFED_GLOBAL_3HOURLY_FRAC_CONV_FN_ROOT="${GFED_OUTPUT_VERSION}_${MODEL_YEAR}_N2O_3hourly_fractions"
GFED_GLOBAL_3HOURLY_FRAC_CONV_FN="${GFED_GLOBAL_3HOURLY_FRAC_CONV_FN_ROOT}.${NETCDF_EXT_NCL}"
export GFED_GLOBAL_3HOURLY_FRAC_CONV_FP="${WORK_DIR}/${GFED_GLOBAL_3HOURLY_FRAC_CONV_FN}"
## file/global attributes
export GFED_GLOBAL_3HOURLY_FRAC_CONV_HISTORY="see ${PROJECT_WEBSITE}"
export GFED_GLOBAL_3HOURLY_FRAC_CONV_SOURCE_FILE="assembled from data from ${GFED_GLOBAL_3HOURLY_FRAC_GZ_URI}"
export GFED_GLOBAL_3HOURLY_FRAC_CONV_TITLE="3hourly GFED N2O fractions for ${MODEL_YEAR}"

## TODO: figure out how to make `raster` handle/regrid 4D datavars.
## Meanwhile, tell it how to find dim=TSTEP. Note
## * R uses Fortran-style indexing
## * netCDF reverses the dimensions under the hood
## , so `float Fraction_of_Emissions(TSTEP, LAY, ROW, COL)` -> position in the dimension array(TSTEP)==4
export GFED_GLOBAL_3HOURLY_FRAC_CONV_TSTEP_POSN='4'

## datavar
export GFED_GLOBAL_3HOURLY_FRAC_CONV_DATAVAR_NAME="${GFED_GLOBAL_3HOURLY_FRAC_DATAVAR_NAME}"
export GFED_GLOBAL_3HOURLY_FRAC_CONV_DATAVAR_UNITS='unitless'
export GFED_GLOBAL_3HOURLY_FRAC_CONV_DATAVAR_LONG_NAME='3-hourly fraction of daily N2O emission'
# datavar dims=(TSTEP, LAY, ROW, COL)
export GFED_GLOBAL_3HOURLY_FRAC_CONV_N_TSTEP="${MONTHS_PER_YEAR}"
export GFED_GLOBAL_3HOURLY_FRAC_CONV_N_LAYER='8' # 8 3-hour periods per file
# still global -> same rows, cols as input .txt
export GFED_GLOBAL_3HOURLY_FRAC_CONV_N_ROW="${GFED_GLOBAL_MONTHLY_TXT_ROWS}"
export GFED_GLOBAL_3HOURLY_FRAC_CONV_N_COL="${GFED_GLOBAL_MONTHLY_TXT_COLS}"

# datavar dims and attributes
export GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_TIME_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_TIME_NAME}"
export GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_TIME_UNITS="${GFED_GLOBAL_MONTHLY_CONV_DIM_TIME_UNITS}"
export GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_TIME_LONG_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_TIME_LONG_NAME}"
export GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_LAYER_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_LAYER_NAME}"
export GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_LAYER_LONG_NAME='hour of day starting 3-hour-long period'
export GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_LAYER_UNITS='hour of day'
export GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_X_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_X_NAME}"
export GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_X_UNITS="${GFED_GLOBAL_MONTHLY_CONV_DIM_X_UNITS}"
export GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_X_LONG_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_X_LONG_NAME}"
export GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_Y_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_Y_NAME}"
export GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_Y_UNITS="${GFED_GLOBAL_MONTHLY_CONV_DIM_Y_UNITS}"
export GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_Y_LONG_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_Y_LONG_NAME}"

### intermediate product: global->AQMEII regridding of aggregated, molarized monthly emissions
GFED_REGRID_MONTHLY_FN_ROOT="${GFED_GLOBAL_MONTHLY_CONV_FN_ROOT}_regrid"
GFED_REGRID_MONTHLY_FN="${GFED_REGRID_MONTHLY_FN_ROOT}.${NETCDF_EXT_NCL}"
export GFED_REGRID_MONTHLY_FP="${WORK_DIR}/${GFED_REGRID_MONTHLY_FN}"
## file/global attributes
export GFED_REGRID_MONTHLY_HISTORY="${GFED_GLOBAL_MONTHLY_CONV_HISTORY}"
export GFED_REGRID_MONTHLY_SOURCE_FILE="${GFED_GLOBAL_MONTHLY_CONV_SOURCE_FILE}"
export GFED_REGRID_MONTHLY_TITLE="${GFED_GLOBAL_MONTHLY_CONV_TITLE}"

## datavar
export GFED_REGRID_MONTHLY_DATAVAR_NAME="${GFED_GLOBAL_MONTHLY_CONV_DATAVAR_NAME}"
export GFED_REGRID_MONTHLY_DATAVAR_UNITS="${GFED_GLOBAL_MONTHLY_CONV_DATAVAR_UNITS}"
export GFED_REGRID_MONTHLY_DATAVAR_LONG_NAME="${GFED_GLOBAL_MONTHLY_CONV_DATAVAR_LONG_NAME}"
# datavar dims=(TSTEP, LAY, ROW, COL)
export GFED_REGRID_MONTHLY_N_TSTEP="${GFED_GLOBAL_MONTHLY_CONV_N_TSTEP}"
export GFED_REGRID_MONTHLY_N_LAYER="${GFED_GLOBAL_MONTHLY_CONV_N_LAYER}"
# no longer global
# export GFED_REGRID_MONTHLY_N_ROW=""
# export GFED_REGRID_MONTHLY_N_COL=""

# datavar dims and attributes
export GFED_REGRID_MONTHLY_DIM_TIME_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_TIME_NAME}"
export GFED_REGRID_MONTHLY_DIM_TIME_UNITS="${GFED_GLOBAL_MONTHLY_CONV_DIM_TIME_UNITS}"
export GFED_REGRID_MONTHLY_DIM_TIME_LONG_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_TIME_LONG_NAME}"
export GFED_REGRID_MONTHLY_DIM_LAYER_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_LAYER_NAME}"
export GFED_REGRID_MONTHLY_DIM_LAYER_LONG_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_LAYER_LONG_NAME}"
export GFED_REGRID_MONTHLY_DIM_LAYER_UNITS="${GFED_GLOBAL_MONTHLY_CONV_DIM_LAYER_UNITS}"
export GFED_REGRID_MONTHLY_DIM_X_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_X_NAME}"
export GFED_REGRID_MONTHLY_DIM_X_UNITS="${GFED_GLOBAL_MONTHLY_CONV_DIM_X_UNITS}"
export GFED_REGRID_MONTHLY_DIM_X_LONG_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_X_LONG_NAME}"
export GFED_REGRID_MONTHLY_DIM_Y_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_Y_NAME}"
export GFED_REGRID_MONTHLY_DIM_Y_UNITS="${GFED_GLOBAL_MONTHLY_CONV_DIM_Y_UNITS}"
export GFED_REGRID_MONTHLY_DIM_Y_LONG_NAME="${GFED_GLOBAL_MONTHLY_CONV_DIM_Y_LONG_NAME}"

### intermediate product: global->AQMEII regridding of aggregated daily fractions
GFED_REGRID_DAILY_FRAC_FN_ROOT="${GFED_GLOBAL_DAILY_FRAC_CONV_FN_ROOT}_regrid"
GFED_REGRID_DAILY_FRAC_FN="${GFED_REGRID_DAILY_FRAC_FN_ROOT}.${NETCDF_EXT_NCL}"
export GFED_REGRID_DAILY_FRAC_FP="${WORK_DIR}/${GFED_REGRID_DAILY_FRAC_FN}"
## file/global attributes
export GFED_REGRID_DAILY_FRAC_HISTORY="${GFED_GLOBAL_DAILY_FRAC_CONV_HISTORY}"
export GFED_REGRID_DAILY_FRAC_SOURCE_FILE="${GFED_GLOBAL_DAILY_FRAC_CONV_SOURCE_FILE}"
export GFED_REGRID_DAILY_FRAC_TITLE="${GFED_GLOBAL_DAILY_FRAC_CONV_TITLE}"

## datavar
export GFED_REGRID_DAILY_FRAC_DATAVAR_NAME="${GFED_GLOBAL_DAILY_FRAC_CONV_DATAVAR_NAME}"
export GFED_REGRID_DAILY_FRAC_DATAVAR_UNITS="${GFED_GLOBAL_DAILY_FRAC_CONV_DATAVAR_UNITS}"
export GFED_REGRID_DAILY_FRAC_DATAVAR_LONG_NAME="${GFED_GLOBAL_DAILY_FRAC_CONV_DATAVAR_LONG_NAME}"
# datavar dims=(TSTEP, LAY, ROW, COL)
export GFED_REGRID_DAILY_FRAC_N_TSTEP="${GFED_GLOBAL_DAILY_FRAC_CONV_N_TSTEP}"
export GFED_REGRID_DAILY_FRAC_N_LAYER="${GFED_GLOBAL_DAILY_FRAC_CONV_N_LAYER}"
# no longer global
# export GFED_REGRID_DAILY_FRAC_N_ROW=""
# export GFED_REGRID_DAILY_FRAC_N_COL=""

# datavar dims and attributes
export GFED_REGRID_DAILY_FRAC_DIM_TIME_NAME="${GFED_GLOBAL_DAILY_FRAC_CONV_DIM_TIME_NAME}"
export GFED_REGRID_DAILY_FRAC_DIM_TIME_UNITS="${GFED_GLOBAL_DAILY_FRAC_CONV_DIM_TIME_UNITS}"
export GFED_REGRID_DAILY_FRAC_DIM_TIME_LONG_NAME="${GFED_GLOBAL_DAILY_FRAC_CONV_DIM_TIME_LONG_NAME}"
export GFED_REGRID_DAILY_FRAC_DIM_LAYER_NAME="${GFED_GLOBAL_DAILY_FRAC_CONV_DIM_LAYER_NAME}"
export GFED_REGRID_DAILY_FRAC_DIM_LAYER_LONG_NAME="${GFED_GLOBAL_DAILY_FRAC_CONV_DIM_LAYER_LONG_NAME}"
export GFED_REGRID_DAILY_FRAC_DIM_X_NAME="${GFED_GLOBAL_DAILY_FRAC_CONV_DIM_X_NAME}"
export GFED_REGRID_DAILY_FRAC_DIM_X_UNITS="${GFED_GLOBAL_DAILY_FRAC_CONV_DIM_X_UNITS}"
export GFED_REGRID_DAILY_FRAC_DIM_X_LONG_NAME="${GFED_GLOBAL_DAILY_FRAC_CONV_DIM_X_LONG_NAME}"
export GFED_REGRID_DAILY_FRAC_DIM_Y_NAME="${GFED_GLOBAL_DAILY_FRAC_CONV_DIM_Y_NAME}"
export GFED_REGRID_DAILY_FRAC_DIM_Y_UNITS="${GFED_GLOBAL_DAILY_FRAC_CONV_DIM_Y_UNITS}"
export GFED_REGRID_DAILY_FRAC_DIM_Y_LONG_NAME="${GFED_GLOBAL_DAILY_FRAC_CONV_DIM_Y_LONG_NAME}"

### intermediate product: global->AQMEII regridding of aggregated 3hourly fractions
GFED_REGRID_3HOURLY_FRAC_FN_ROOT="${GFED_GLOBAL_3HOURLY_FRAC_CONV_FN_ROOT}_regrid"
GFED_REGRID_3HOURLY_FRAC_FN="${GFED_REGRID_3HOURLY_FRAC_FN_ROOT}.${NETCDF_EXT_NCL}"
export GFED_REGRID_3HOURLY_FRAC_FP="${WORK_DIR}/${GFED_REGRID_3HOURLY_FRAC_FN}"
## file/global attributes
export GFED_REGRID_3HOURLY_FRAC_HISTORY="${GFED_GLOBAL_3HOURLY_FRAC_CONV_HISTORY}"
export GFED_REGRID_3HOURLY_FRAC_SOURCE_FILE="${GFED_GLOBAL_3HOURLY_FRAC_CONV_SOURCE_FILE}"
export GFED_REGRID_3HOURLY_FRAC_TITLE="${GFED_GLOBAL_3HOURLY_FRAC_CONV_TITLE}"

## datavar
export GFED_REGRID_3HOURLY_FRAC_DATAVAR_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DATAVAR_NAME}"
export GFED_REGRID_3HOURLY_FRAC_DATAVAR_UNITS="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DATAVAR_UNITS}"
export GFED_REGRID_3HOURLY_FRAC_DATAVAR_LONG_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DATAVAR_LONG_NAME}"
# datavar dims=(TSTEP, LAY, ROW, COL)
export GFED_REGRID_3HOURLY_FRAC_N_TSTEP="${GFED_GLOBAL_3HOURLY_FRAC_CONV_N_TSTEP}"
export GFED_REGRID_3HOURLY_FRAC_N_LAYER="${GFED_GLOBAL_3HOURLY_FRAC_CONV_N_LAYER}"

# datavar dims and attributes
export GFED_REGRID_3HOURLY_FRAC_DIM_TIME_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_TIME_NAME}"
export GFED_REGRID_3HOURLY_FRAC_DIM_TIME_UNITS="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_TIME_UNITS}"
export GFED_REGRID_3HOURLY_FRAC_DIM_TIME_LONG_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_TIME_LONG_NAME}"
export GFED_REGRID_3HOURLY_FRAC_DIM_LAYER_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_LAYER_NAME}"
export GFED_REGRID_3HOURLY_FRAC_DIM_LAYER_LONG_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_LAYER_LONG_NAME}"
export GFED_REGRID_3HOURLY_FRAC_DIM_LAYER_UNITS="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_LAYER_UNITS}"
export GFED_REGRID_3HOURLY_FRAC_DIM_X_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_X_NAME}"
export GFED_REGRID_3HOURLY_FRAC_DIM_X_UNITS="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_X_UNITS}"
export GFED_REGRID_3HOURLY_FRAC_DIM_X_LONG_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_X_LONG_NAME}"
export GFED_REGRID_3HOURLY_FRAC_DIM_Y_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_Y_NAME}"
export GFED_REGRID_3HOURLY_FRAC_DIM_Y_UNITS="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_Y_UNITS}"
export GFED_REGRID_3HOURLY_FRAC_DIM_Y_LONG_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_Y_LONG_NAME}"

## TODO: figure out how to make `raster` handle/regrid 4D datavars.
## Meanwhile, tell it how to find dim=TSTEP ...
export GFED_REGRID_3HOURLY_FRAC_TSTEP_POSN="${GFED_GLOBAL_3HOURLY_FRAC_CONV_TSTEP_POSN}"
## ... and give it template for multiple netCDF output files :-(
# export GFED_REGRID_3HOURLY_FRAC_TEMPLATE_STRING="${GFED_GLOBAL_3HOURLY_FRAC_TEMPLATE_STRING}"
GFED_REGRID_3HOURLY_FRAC_FN_TEMPLATE="${GFED_REGRID_3HOURLY_FRAC_FN_ROOT}_${TEMPLATE_STRING}.${NETCDF_EXT_NCL}"
export GFED_REGRID_3HOURLY_FRAC_FP_TEMPLATE="${WORK_DIR}/${GFED_REGRID_3HOURLY_FRAC_FN_TEMPLATE}"
# just the 3-hour regrids for January
export GFED_REGRID_3HOURLY_FRAC_JAN_FP="${GFED_REGRID_3HOURLY_FRAC_FP_TEMPLATE/${TEMPLATE_STRING}/${JANUARY_STRING}}"

## plot intermediate product: netCDFed aggregation of molarized GFED global monthly emissions
GFED_GLOBAL_MONTHLY_CONV_PDF_FN="${GFED_GLOBAL_MONTHLY_CONV_FN_ROOT}_$(date +${DATE_FORMAT}).pdf"
export GFED_GLOBAL_MONTHLY_CONV_PDF_FP="${WORK_DIR}/${GFED_GLOBAL_MONTHLY_CONV_PDF_FN}" # path to PDF output
export GFED_GLOBAL_MONTHLY_CONV_PDF_HEIGHT='25'
export GFED_GLOBAL_MONTHLY_CONV_PDF_WIDTH='5'

## plot intermediate product: regridded aggregated molarized monthly emissions
GFED_REGRID_MONTHLY_PDF_FN="${GFED_REGRID_MONTHLY_FN_ROOT}_$(date +${DATE_FORMAT}).pdf"
export GFED_REGRID_MONTHLY_PDF_FP="${WORK_DIR}/${GFED_REGRID_MONTHLY_PDF_FN}" # path to PDF output
export GFED_REGRID_MONTHLY_PDF_HEIGHT="${GFED_GLOBAL_MONTHLY_CONV_PDF_HEIGHT}"
export GFED_REGRID_MONTHLY_PDF_WIDTH="${GFED_GLOBAL_MONTHLY_CONV_PDF_WIDTH}"

# ----------------------------------------------------------------------
# final output: hourly emissions over AQMEII
# ----------------------------------------------------------------------

### template for multiple netCDF output files (one per day)
# export GFED_REGRID_HOURLY_EMIS_TEMPLATE_STRING='@@@@@@@@'
# CMAQ wants an extension that NCL won't write
GFED_REGRID_HOURLY_EMIS_FN_TEMPLATE_CMAQ="emis_mole_N2O_${TEMPLATE_STRING}.${NETCDF_EXT_CMAQ}"
GFED_REGRID_HOURLY_EMIS_FN_TEMPLATE_NCL="${GFED_REGRID_HOURLY_EMIS_FN_TEMPLATE_CMAQ}.${NETCDF_EXT_NCL}"
export GFED_REGRID_HOURLY_EMIS_FP_TEMPLATE_CMAQ="${WORK_DIR}/${GFED_REGRID_HOURLY_EMIS_FN_TEMPLATE_CMAQ}"
export GFED_REGRID_HOURLY_EMIS_FP_TEMPLATE_NCL="${WORK_DIR}/${GFED_REGRID_HOURLY_EMIS_FN_TEMPLATE_NCL}"

## just the 3-hour regrids for January 01
export GFED_REGRID_HOURLY_EMIS_JAN_01_FP_CMAQ="${GFED_REGRID_HOURLY_EMIS_FP_TEMPLATE_CMAQ}/${TEMPLATE_STRING}/${JANUARY_FIRST_STRING}}"
export GFED_REGRID_HOURLY_EMIS_JAN_01_FP_NCL="${GFED_REGRID_HOURLY_EMIS_FP_TEMPLATE_NCL}/${TEMPLATE_STRING}/${JANUARY_FIRST_STRING}}"

## file/global attributes
export GFED_REGRID_HOURLY_EMIS_ATTR_HISTORY="see ${PROJECT_WEBSITE}"
export GFED_REGRID_HOURLY_EMIS_ATTR_TITLE='hourly N2O emissions due to biomass burning derived from GFED emissions and fractions over AQMEII-NA in mol/s, IOAPI-zed'
export GFED_REGRID_HOURLY_EMIS_ATTR_FILEDESC="${GFED_REGRID_HOURLY_EMIS_ATTR_TITLE}"

### datavar
export GFED_REGRID_HOURLY_EMIS_DATAVAR_NAME='N2O'
## format datavar attributes IOAPI-style
# IOAPI pads varattr=units to length=16 with trailing spaces
export GFED_REGRID_HOURLY_EMIS_DATAVAR_ATTR_UNITS="$(printf '%-16s' 'moles/s')"
# IOAPI pads varattr=long_name to length=16 with trailing spaces
export GFED_REGRID_HOURLY_EMIS_DATAVAR_ATTR_LONG_NAME="$(printf '%-16s' ${GFED_REGRID_HOURLY_EMIS_DATAVAR_NAME})"
# IOAPI pads varattr=var_desc to length=80 with trailing spaces
export GFED_REGRID_HOURLY_EMIS_DATAVAR_ATTR_VAR_DESC="$(printf '%-80s' ${GFED_REGRID_HOURLY_EMIS_DATAVAR_NAME})"

# datavar dims=(TSTEP, LAY, ROW, COL)
export GFED_REGRID_HOURLY_EMIS_N_TSTEP="$(( HOURS_PER_DAY+1 ))"
export GFED_REGRID_HOURLY_EMIS_N_LAYER='1' # one species in file

# # datavar dims and attributes
# export GFED_REGRID_HOURLY_EMIS_DIM_TIME_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_TIME_NAME}"
# export GFED_REGRID_HOURLY_EMIS_DIM_TIME_UNITS="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_TIME_UNITS}"
# export GFED_REGRID_HOURLY_EMIS_DIM_TIME_LONG_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_TIME_LONG_NAME}"
# export GFED_REGRID_HOURLY_EMIS_DIM_LAYER_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_LAYER_NAME}"
# export GFED_REGRID_HOURLY_EMIS_DIM_LAYER_LONG_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_LAYER_LONG_NAME}"
# export GFED_REGRID_HOURLY_EMIS_DIM_LAYER_UNITS="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_LAYER_UNITS}"
# export GFED_REGRID_HOURLY_EMIS_DIM_X_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_X_NAME}"
# export GFED_REGRID_HOURLY_EMIS_DIM_X_UNITS="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_X_UNITS}"
# export GFED_REGRID_HOURLY_EMIS_DIM_X_LONG_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_X_LONG_NAME}"
# export GFED_REGRID_HOURLY_EMIS_DIM_Y_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_Y_NAME}"
# export GFED_REGRID_HOURLY_EMIS_DIM_Y_UNITS="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_Y_UNITS}"
# export GFED_REGRID_HOURLY_EMIS_DIM_Y_LONG_NAME="${GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_Y_LONG_NAME}"

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# setup functions
# ----------------------------------------------------------------------

# TODO: test for resources, reuse if available
# `setup_paths` isa bash util, so `get_helpers` first
# `setup_apps` ditto
function setup {
  for CMD in \
    "mkdir -p ${WORK_DIR}" \
    'get_helpers' \
    'setup_paths' \
    'setup_apps' \
    'setup_resources' \
  ; do
    echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
      exit 1
    fi
  done
#  echo -e "\n$ ${THIS_FN}: PDF_VIEWER='${PDF_VIEWER}'" # debugging
} # end function setup

function get_helpers {
  for CMD in \
    'get_regrid_utils' \
    'get_bash_utils' \
    'get_check_raw_fracs' \
    'get_get_input_areas' \
    'get_conv_monthlies' \
    'get_time_funcs' \
    'get_conv_dailies' \
    'get_conv_3hourlies' \
    'get_IOAPI_funcs' \
    'get_stats_funcs' \
    'get_vis_funcs' \
    'get_vis_regrid_vis' \
    'get_fix_3hourlies' \
    'get_check_coord_vars' \
    'get_filepath_funcs_fp' \
    'get_get_output_areas' \
    'get_make_hourlies' \
    'get_string_funcs' \
    'get_summarize' \
    'get_check_conservation' \
  ; do
    echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
      exit 1
    fi
  done
} # end function get_helpers

# This gets "package"=regrid_utils from which the remaining helpers are (at least potentially) drawn.
function get_regrid_utils {
  if [[ -z "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: REGRID_UTILS_DIR not defined"
    exit 3
  fi

  # Noticed a problem where code committed to regrid_utils was not replacing code in existing ${REGRID_UTILS_DIR}, so ...
  if [[ -r "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: removing existing regrid_utils folder='${REGRID_UTILS_DIR}'"
    for CMD in \
      "rm -fr ${REGRID_UTILS_DIR}/" \
     ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 4
      fi
    done
  fi

  if [[ ! -r "${REGRID_UTILS_DIR}" ]] ; then
  # assumes GIT_CLONE_PREFIX set in environment by, e.g., uber_driver
    for CMD in \
      "${GIT_CLONE_PREFIX} git clone ${REGRID_UTILS_URI}.git" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo # newline
        echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found"
        echo -e "\t(suggestion: check that GIT_CLONE_PREFIX is set appropriately in calling environment)"
        echo # newline
        exit 5
      fi
    done
  fi

  if [[ ! -r "${REGRID_UTILS_DIR}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot download regrid_utils to '${REGRID_UTILS_DIR}'"
    exit 6
  fi  
} # end function get_regrid_utils

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${BASH_UTILS_FP} before running this script.
function get_bash_utils {
  if [[ -z "${BASH_UTILS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BASH_UTILS_FP not defined"
    exit 5
  fi
  if [[ ! -r "${BASH_UTILS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${BASH_UTILS_FN} ${BASH_UTILS_FP}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 1
      fi
    done
  fi
  if [[ ! -r "${BASH_UTILS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: BASH_UTILS_FP=='${BASH_UTILS_FP}' not readable"
    exit 6
  fi
  # This is bash, so gotta ...
  source "${BASH_UTILS_FP}"
  # ... for its functions to be available later in this script
} # end function get_bash_utils

function get_check_raw_fracs {
  if [[ -z "${CHECK_RAW_FRACS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CHECK_RAW_FRACS_FP not defined"
    exit 1
  fi
  # is in this repo
#  if [[ ! -r "${CHECK_RAW_FRACS_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${CHECK_RAW_FRACS_FP} ${CHECK_RAW_FRACS_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${CHECK_RAW_FRACS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CHECK_RAW_FRACS_FP=='${CHECK_RAW_FRACS_FP}' not readable"
    exit 1
  fi
} # end function get_check_raw_fracs

function get_get_input_areas {
  if [[ -z "${GET_INPUT_AREAS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: GET_INPUT_AREAS_FP not defined"
    exit 1
  fi
  if [[ ! -r "${GET_INPUT_AREAS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${GET_INPUT_AREAS_FN} ${GET_INPUT_AREAS_FP}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 13
      fi
    done
  fi
  if [[ ! -r "${GET_INPUT_AREAS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: GET_INPUT_AREAS_FP=='${GET_INPUT_AREAS_FP}' not readable"
    exit 1
  fi
} # end function get_get_input_areas

function get_get_output_areas {
  if [[ -z "${GET_OUTPUT_AREAS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: GET_OUTPUT_AREAS_FP not defined"
    exit 1
  fi
  if [[ ! -r "${GET_OUTPUT_AREAS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${GET_OUTPUT_AREAS_FN} ${GET_OUTPUT_AREAS_FP}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 13
      fi
    done
  fi
  if [[ ! -r "${GET_OUTPUT_AREAS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: GET_OUTPUT_AREAS_FP=='${GET_OUTPUT_AREAS_FP}' not readable"
    exit 1
  fi
} # end function get_get_output_areas

function get_filepath_funcs_fp {
  if [[ -z "${FILEPATH_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: FILEPATH_FUNCS_FP not defined"
    exit 1
  fi
  if [[ ! -r "${FILEPATH_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${FILEPATH_FUNCS_FN} ${FILEPATH_FUNCS_FP}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 13
      fi
    done
  fi
  if [[ ! -r "${FILEPATH_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: FILEPATH_FUNCS_FP=='${FILEPATH_FUNCS_FP}' not readable"
    exit 1
  fi
} # end function get_filepath_funcs_fp

function get_conv_monthlies {
  if [[ -z "${CONV_MONTHLIES_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CONV_MONTHLIES_FP not defined"
    exit 1
  fi
  # is in this repo
#  if [[ ! -r "${CONV_MONTHLIES_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${CONV_MONTHLIES_FP} ${CONV_MONTHLIES_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${CONV_MONTHLIES_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CONV_MONTHLIES_FP=='${CONV_MONTHLIES_FP}' not readable"
    exit 1
  fi
} # end function get_conv_monthlies

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${TIME_FUNCS_FP} before running this script.
function get_time_funcs {
  if [[ -z "${TIME_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: TIME_FUNCS_FP not defined"
    exit 15
  fi
  if [[ ! -r "${TIME_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${TIME_FUNCS_FN} ${TIME_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 1
      fi
    done
  fi
  if [[ ! -r "${TIME_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: TIME_FUNCS_FP=='${TIME_FUNCS_FP}' not readable"
    exit 16
  fi
} # end function get_time_funcs

function get_conv_dailies {
  if [[ -z "${CONV_DAILIES_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CONV_DAILIES_FP not defined"
    exit 1
  fi
  # is in this repo
#  if [[ ! -r "${CONV_DAILIES_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${CONV_DAILIES_FP} ${CONV_DAILIES_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${CONV_DAILIES_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CONV_DAILIES_FP=='${CONV_DAILIES_FP}' not readable"
    exit 1
  fi
} # end function get_conv_dailies

function get_conv_3hourlies {
  if [[ -z "${CONV_3HOURLIES_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CONV_3HOURLIES_FP not defined"
    exit 1
  fi
  # is in this repo
#  if [[ ! -r "${CONV_3HOURLIES_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${CONV_3HOURLIES_FP} ${CONV_3HOURLIES_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${CONV_3HOURLIES_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CONV_3HOURLIES_FP=='${CONV_3HOURLIES_FP}' not readable"
    exit 1
  fi
} # end function get_conv_3hourlies

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${IOAPI_FUNCS_FP} before running this script.
function get_IOAPI_funcs {
  if [[ -z "${IOAPI_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}: ERROR: IOAPI_FUNCS_FP not defined"
    exit 7
  fi
  if [[ ! -r "${IOAPI_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${IOAPI_FUNCS_FN} ${IOAPI_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
    done
  fi
  if [[ ! -r "${IOAPI_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: IOAPI_FUNCS_FP=='${IOAPI_FUNCS_FP}' not readable"
    exit 8
  fi
} # end function get_IOAPI_funcs

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${STATS_FUNCS_FP} before running this script.
function get_stats_funcs {
  if [[ -z "${STATS_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: STATS_FUNCS_FP not defined"
    exit 9
  fi
  if [[ ! -r "${STATS_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${STATS_FUNCS_FN} ${STATS_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 1
      fi
    done
  fi
  if [[ ! -r "${STATS_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: STATS_FUNCS_FP=='${STATS_FUNCS_FP}' not readable"
    exit 10
  fi
} # end function get_stats_funcs

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${STRING_FUNCS_FP} before running this script.
function get_string_funcs {
  if [[ -z "${STRING_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: STRING_FUNCS_FP not defined"
    exit 9
  fi
  if [[ ! -r "${STRING_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${STRING_FUNCS_FN} ${STRING_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 1
      fi
    done
  fi
  if [[ ! -r "${STRING_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: STRING_FUNCS_FP=='${STRING_FUNCS_FP}' not readable"
    exit 10
  fi
} # end function get_string_funcs

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${SUMMARIZE_FUNCS_FP} before running this script.
function get_summarize {
  if [[ -z "${SUMMARIZE_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: SUMMARIZE_FUNCS_FP not defined"
    exit 21
  fi
  if [[ ! -r "${SUMMARIZE_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${SUMMARIZE_FUNCS_FN} ${SUMMARIZE_FUNCS_FP}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 22
      fi
    done
  fi
  if [[ ! -r "${SUMMARIZE_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: SUMMARIZE_FUNCS_FP=='${SUMMARIZE_FUNCS_FP}' not readable"
    exit 23
  fi
} # end function get_summarize

# isa regrid_utils from https://bitbucket.org/epa_n2o_project_team/regrid_utils
# To override, copy/mod to ${VIS_FUNCS_FP} before running this script.
function get_vis_funcs {
  if [[ -z "${VIS_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: VIS_FUNCS_FP not defined"
    exit 17
  fi
  if [[ ! -r "${VIS_FUNCS_FP}" ]] ; then
    # copy from downloaded regrid_utils
    for CMD in \
      "cp ${REGRID_UTILS_DIR}/${VIS_FUNCS_FN} ${VIS_FUNCS_FP}" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 1
      fi
    done
  fi
  if [[ ! -r "${VIS_FUNCS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: VIS_FUNCS_FP=='${VIS_FUNCS_FP}' not readable"
    exit 18
  fi
} # end function get_vis_funcs

function get_vis_regrid_vis {
  if [[ -z "${VIS_REGRID_VIS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: VIS_REGRID_VIS_FP not defined"
    exit 1
  fi
  # is in this repo
#  if [[ ! -r "${VIS_REGRID_VIS_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${VIS_REGRID_VIS_FP} ${VIS_REGRID_VIS_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${VIS_REGRID_VIS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: VIS_REGRID_VIS_FP=='${VIS_REGRID_VIS_FP}' not readable"
    exit 1
  fi
} # end function get_vis_regrid_vis

function get_fix_3hourlies {
  if [[ -z "${FIX_3HOURLIES_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: FIX_3HOURLIES_FP not defined"
    exit 1
  fi
  # is in this repo
#  if [[ ! -r "${FIX_3HOURLIES_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${FIX_3HOURLIES_FP} ${FIX_3HOURLIES_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${FIX_3HOURLIES_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: FIX_3HOURLIES_FP=='${FIX_3HOURLIES_FP}' not readable"
    exit 1
  fi
} # end function get_fix_3hourlies

function get_check_coord_vars {
  if [[ -z "${CHECK_COORD_VARS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CHECK_COORD_VARS_FP not defined"
    exit 1
  fi
  # is in this repo
#  if [[ ! -r "${CHECK_COORD_VARS_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${CHECK_COORD_VARS_FP} ${CHECK_COORD_VARS_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${CHECK_COORD_VARS_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CHECK_COORD_VARS_FP=='${CHECK_COORD_VARS_FP}' not readable"
    exit 1
  fi
} # end function get_check_coord_vars

function get_make_hourlies {
  if [[ -z "${MAKE_HOURLIES_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: MAKE_HOURLIES_FP not defined"
    exit 1
  fi
  # is in this repo
#  if [[ ! -r "${MAKE_HOURLIES_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${MAKE_HOURLIES_FP} ${MAKE_HOURLIES_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${MAKE_HOURLIES_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: MAKE_HOURLIES_FP=='${MAKE_HOURLIES_FP}' not readable"
    exit 1
  fi
} # end function get_make_hourlies

function get_check_conservation {
  if [[ -z "${CHECK_CONSERVATION_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CHECK_CONSERVATION_FP not defined"
    exit 1
  fi
  # is in this repo
#  if [[ ! -r "${CHECK_CONSERVATION_FP}" ]] ; then
#    for CMD in \
#      "${WGET_TO_FILE} ${CHECK_CONSERVATION_FP} ${CHECK_CONSERVATION_URI}" \
#    ; do
#      echo -e "$ ${CMD}"
#      eval "${CMD}"
#    done
#  fi
  if [[ ! -r "${CHECK_CONSERVATION_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: CHECK_CONSERVATION_FP=='${CHECK_CONSERVATION_FP}' not readable"
    exit 1
  fi
} # end function get_check_conservation

function setup_resources {
  for CMD in \
    'get_template_input' \
    'get_output_areas' \
    'download_GFED' \
  ; do
    echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
      exit 1
    fi
  done
} # end function setup_resources

### get the "template" file used for regridding and IOAPI-writing
function get_template_input {
  if [[ -z "${TEMPLATE_INPUT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: TEMPLATE_INPUT_FP not defined"
    exit 8
  fi
  if [[ ! -r "${TEMPLATE_INPUT_FP}" ]] ; then
    if [[ ! -r "${TEMPLATE_INPUT_GZ_FP}" ]] ; then
      for CMD in \
        "${WGET_TO_FILE} ${TEMPLATE_INPUT_GZ_FP} ${TEMPLATE_INPUT_GZ_URI}" \
      ; do
        echo -e "$ ${CMD}"
        eval "${CMD}"
      done
    fi
    if [[ -r "${TEMPLATE_INPUT_GZ_FP}" ]] ; then
      # JIC ${TEMPLATE_INPUT_FP} != ${TEMPLATE_INPUT_GZ_FP} - .gz
      for CMD in \
        "gunzip -c ${TEMPLATE_INPUT_GZ_FP} > ${TEMPLATE_INPUT_FP}" \
      ; do
        echo -e "$ ${CMD}"
        eval "${CMD}"
      done
    fi
  fi
  if [[ ! -r "${TEMPLATE_INPUT_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot read TEMPLATE_INPUT_FP=='${TEMPLATE_INPUT_FP}'"
    exit 9
  fi
} # end function get_template_input

### Call NCL helper to get areas of output/AQMEII-NA gridcells.
### Depends on functions={get_get_output_areas, get_MSFs} above.
### Assumes `ncl` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to NCL
### for now: just use envvars :-(
function get_output_areas {
# get_MSFs
# ncl # bail to console, or
  if [[ ! -r "${AQMEIINA_AREAS_FP}" ]] ; then
    for CMD in \
      'get_MSFs' \
      "ncl -n ${GET_OUTPUT_AREAS_FP}" \
    ; do
    cat <<EOM

${THIS_FN}::${FUNCNAME[0]}: about to run command='${CMD}'.

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 1
      fi
    done
  fi
} # end function get_output_areas

function download_GFED {
  for CMD in \
    'download_GFED_monthly_emissions' \
    'download_GFED_daily_fractions' \
    'download_GFED_3hour_fractions' \
  ; do
    echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}"
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
      exit 1
    fi
  done
} # end function download_GFED

function download_GFED_monthly_emissions {
  if [[
    -z "${CURL_TO_FILE}"
    || -z "${GFED_GLOBAL_MONTHLY_ZIP_FP}"
    || -z "${GFED_GLOBAL_MONTHLY_ZIP_URI}"
    || -z "${GFED_GLOBAL_MONTHLY_TEMPLATE}"
    || -z "${GFED_GLOBAL_MONTHLY_JAN_FP}"
    || -z "${WORK_DIR}"
  ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: undefined dependencies"
#    echo -e "${THIS_FN}::${FUNCNAME[0]}: GFED_GLOBAL_MONTHLY_JAN_FP=='${GFED_GLOBAL_MONTHLY_JAN_FP}'"
    exit 1
  fi

  ## if we don't have the all the monthly .txt ... err, just check January
  if [[ ! -r "${GFED_GLOBAL_MONTHLY_JAN_FP}" ]] ; then
    for CMD in \
      "${CURL_TO_FILE} ${GFED_GLOBAL_MONTHLY_ZIP_FP} ${GFED_GLOBAL_MONTHLY_ZIP_URI}" \
      "unzip ${GFED_GLOBAL_MONTHLY_ZIP_FP} '${GFED_GLOBAL_MONTHLY_TEMPLATE}' -d ${WORK_DIR}" \
      "ls -alh ${WORK_DIR}/${GFED_GLOBAL_MONTHLY_TEMPLATE}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 1
      fi
    done
  fi

  ## if we still don't have them, die ...
  if [[ ! -r "${GFED_GLOBAL_MONTHLY_JAN_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: could not download monthly emissions, e.g., '${GFED_GLOBAL_MONTHLY_JAN_FP}'"
    exit 1
  fi
} # end function download_GFED

function download_GFED_daily_fractions {
  if [[
    -z "${CURL_TO_STREAM}"
    || -z "${GFED_GLOBAL_DAILY_FRAC_GZ_URI}"
    || -z "${GFED_GLOBAL_DAILY_FRAC_TEMPLATE}"
    || -z "${GFED_GLOBAL_DAILY_FRAC_JAN_01_FP}"
    || -z "${GFED_GLOBAL_DAILY_FRAC_GZ_FAKE_FP}"
    || -z "${GFED_GLOBAL_DAILY_FRAC_GZ_FN}"
    || -z "${MODEL_YEAR}"
  ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: undefined dependencies"
    exit 1
  fi

  ## if we don't have the all the daily *.nc ... err, just check 1 Jan
  if [[ ! -r "${GFED_GLOBAL_DAILY_FRAC_JAN_01_FP}" ]] ; then

    # too many options :-( Formerly I just downloaded from GFED aka zea.ess.uci.edu, but it's down too often.
    # So first check to see if the archive file is available locally:
    if [[ ! -r "${GFED_GLOBAL_DAILY_FRAC_GZ_FAKE_FP}" ]] ; then

      # download it from GFED. TODO: alternatively download from https://bitbucket.org/epa_n2o_project_team/gfed-3.1_global_to_aqmeii-na/downloads
      for CMD in \
        "${CURL_TO_FILE} ${GFED_GLOBAL_DAILY_FRAC_GZ_URI}" \
        "ls -alh ${GFED_GLOBAL_DAILY_FRAC_GZ_FN}" \
      ; do
        echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
        eval "${CMD}" # comment this out for NOPing, e.g., to `source`
        if [[ $? -ne 0 ]] ; then
          echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
          exit 1
        fi
      done

    fi # end if [[ ! -r "${GFED_GLOBAL_DAILY_FRAC_GZ_FAKE_FP}" ]]
    if [[ ! -r "${GFED_GLOBAL_DAILY_FRAC_GZ_FAKE_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot find or get daily fractions archive='${GFED_GLOBAL_DAILY_FRAC_GZ_FAKE_FP}'"
      exit 1

    else # expand it

      # the dailies will expand to a folder name=${MODEL_YEAR}
      for CMD in \
	"tar xfz ${GFED_GLOBAL_DAILY_FRAC_GZ_FAKE_FP}" \
	"mv ${MODEL_YEAR}/* ./" \
	"rm -fr ${MODEL_YEAR}" \
	"ls -alh ${GFED_GLOBAL_DAILY_FRAC_TEMPLATE}" \
      ; do
	echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
	eval "${CMD}" # comment this out for NOPing, e.g., to `source`
	if [[ $? -ne 0 ]] ; then
	  echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
	  exit 1
	fi
      done

    fi # end if [[ ! -r "${GFED_GLOBAL_DAILY_FRAC_GZ_FAKE_FP}" ]]

  fi # end if [[ ! -r "${GFED_GLOBAL_DAILY_FRAC_JAN_01_FP}" ]]

  ## if we still don't have dailies, die ...
  if [[ ! -r "${GFED_GLOBAL_DAILY_FRAC_JAN_01_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: could not produce daily fractions, e.g., '${GFED_GLOBAL_DAILY_FRAC_JAN_01_FP}'"
    exit 1
  fi
} # end function download_GFED_daily_fractions

function download_GFED_3hour_fractions {
  if [[
    -z "${CURL_TO_STREAM}"
    || -z "${GFED_GLOBAL_3HOURLY_FRAC_GZ_URI}"
    || -z "${GFED_GLOBAL_3HOURLY_FRAC_TEMPLATE}"
    || -z "${GFED_GLOBAL_3HOURLY_FRAC_JAN_FP}"
    || -z "${GFED_GLOBAL_3HOURLY_FRAC_GZ_FAKE_FP}"
    || -z "${GFED_GLOBAL_3HOURLY_FRAC_GZ_FN}"
    || -z "${MODEL_YEAR}"
  ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: undefined dependencies"
    exit 1
  fi

  ## if we don't have the all the 3-hour .nc ... err, just check January
  if [[ ! -r "${GFED_GLOBAL_3HOURLY_FRAC_JAN_FP}" ]] ; then

    # too many options :-( Formerly I just downloaded from GFED aka zea.ess.uci.edu, but it's down too often.
    # So first check to see if the archive file is available locally:
    if [[ ! -r "${GFED_GLOBAL_3HOURLY_FRAC_GZ_FAKE_FP}" ]] ; then

      # download it from GFED. TODO: alternatively download from https://bitbucket.org/epa_n2o_project_team/gfed-3.1_global_to_aqmeii-na/downloads
      for CMD in \
        "${CURL_TO_FILE} ${GFED_GLOBAL_3HOURLY_FRAC_GZ_URI}" \
        "ls -alh ${GFED_GLOBAL_3HOURLY_FRAC_GZ_FN}" \
      ; do
        echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
        eval "${CMD}" # comment this out for NOPing, e.g., to `source`
        if [[ $? -ne 0 ]] ; then
          echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
          exit 1
        fi
      done

    fi # end if [[ ! -r "${GFED_GLOBAL_3HOURLY_FRAC_GZ_FAKE_FP}" ]]
    if [[ ! -r "${GFED_GLOBAL_3HOURLY_FRAC_GZ_FAKE_FP}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot find or get 3-hourly fractions archive='${GFED_GLOBAL_3HOURLY_FRAC_GZ_FAKE_FP}'"
      exit 1

    else # expand it

      # the 3hourlies will expand to a folder name=${MODEL_YEAR}
      for CMD in \
	"tar xfz ${GFED_GLOBAL_3HOURLY_FRAC_GZ_FAKE_FP}" \
	"mv ${MODEL_YEAR}/* ./" \
	"rm -fr ${MODEL_YEAR}" \
	"ls -alh ${GFED_GLOBAL_3HOURLY_FRAC_TEMPLATE}" \
      ; do
	echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
	eval "${CMD}" # comment this out for NOPing, e.g., to `source`
	if [[ $? -ne 0 ]] ; then
	  echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
	  exit 1
	fi
      done

    fi # end if [[ ! -r "${GFED_GLOBAL_3HOURLY_FRAC_GZ_FAKE_FP}" ]]

  fi # end if [[ ! -r "${GFED_GLOBAL_3HOURLY_FRAC_JAN_01_FP}" ]]

  ## if we still don't have 3hourlies, die ...
  if [[ ! -r "${GFED_GLOBAL_3HOURLY_FRAC_JAN_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: could not produce 3-hour fractions, e.g., '${GFED_GLOBAL_3HOURLY_FRAC_JAN_FP}'"
    exit 1
  fi
} # end function download_GFED_3hour_fractions

### Call NCL helper to get areas of input/global gridcells.
### Depends on function get_get_input_areas above.
### Assumes `ncl` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to NCL
### for now: just use envvars :-(
function get_input_areas {
  if [[ ! -r "${GFED_AREAS_GLOBAL_FP}" ]] ; then
    for CMD in \
      "ncl -n ${GET_INPUT_AREAS_FP}" \
    ; do
    cat <<EOM

About to run command='${CMD}'.

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 1
      fi
    done
  fi
} # end function get_input_areas

# ----------------------------------------------------------------------
# action functions
# ----------------------------------------------------------------------

### Call NCL helper to check the sum and distribution of the {raw, as downloaded from GFED} daily and 3-hour fraction values.
### Depends on function get_check_raw_fracs above.
### Assumes `ncl` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to NCL
### for now: just use envvars :-(
function check_raw_fracs {
# ncl # bail to NCL and copy script lines, or ...
  for CMD in \
    "ncl -n ${CHECK_RAW_FRACS_FP}" \
  ; do
  cat <<EOM

About to run command='${CMD}'. WARNING: may seem to hang while processing!

EOM
    eval "${CMD}"
    if [[ $? -ne 0 ]] ; then
      echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
      exit 1
    fi
  done
} # end function check_raw_fracs

### Call NCL helper to convert the 12 {raw, as downloaded from GFED} monthly emissions native.txt files

### * from native .txt format to netCDF
### * to a single netCDF file with 12 TSTEP, for ease of regridding as a single RasterBrick
### * from flux-rate units (explicit timestep) to molar-mass (implicit timestep): i.e., multiply by gridcell areas

### Depends on function get_conv_monthlies above.
### Assumes `ncl` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to NCL
### for now: just use envvars :-(
function conv_monthlies {
# ncl # bail to NCL and copy script lines, or ...
  if [[ ! -r "${GFED_GLOBAL_MONTHLY_CONV_FP}" ]] ; then
    for CMD in \
      "ncl -n ${CONV_MONTHLIES_FP}" \
      "ncdump -h ${GFED_GLOBAL_MONTHLY_CONV_FP}" \
    ; do
    cat <<EOM

About to run command='${CMD}'.

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 1
      fi
    done
  fi
} # end function conv_monthlies

### Call NCL helper to convert the 366 {raw, as downloaded from GFED} daily-fraction netCDF files
### to a single netCDF file with 366 TSTEP, for ease of regridding as a single RasterBrick

### Depends on function get_conv_dailies above.
### Assumes `ncl` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to NCL
### for now: just use envvars :-(
function conv_dailies {
# eval "rm ${GFED_GLOBAL_DAILY_FRAC_CONV_FP}"
# ncl # bail to NCL and copy script lines, or ...
  if [[ ! -r "${GFED_GLOBAL_DAILY_FRAC_CONV_FP}" ]] ; then
    for CMD in \
      "ncl -n ${CONV_DAILIES_FP}" \
      "ncdump -h ${GFED_GLOBAL_DAILY_FRAC_CONV_FP}" \
    ; do
    cat <<EOM

About to run command='${CMD}'. WARNING: may seem to hang while processing!

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 1
      fi
    done
  fi

# `ncdump -h` ->
# netcdf GFED-3.1_2008_N2O_daily_fractions {
# dimensions:
#       TSTEP = 366 ;
#       LAY = 1 ;
#       ROW = 360 ;
#       COL = 720 ;
# variables:
#       float Fraction_of_Emissions(TSTEP, LAY, ROW, COL) ;
#               Fraction_of_Emissions:units = "unitless" ;
#               Fraction_of_Emissions:long_name = "daily fraction of monthly N2O emission" ;
#               Fraction_of_Emissions:_FillValue = 9.96921e+36f ;
#       int TSTEP(TSTEP) ;
#               TSTEP:long_name = "index of day in 2008" ;
#               TSTEP:units = "day" ;
#       int LAY(LAY) ;
#               LAY:units = "unitless" ;
#               LAY:long_name = "index of vertical layers" ;
#       float ROW(ROW) ;
#               ROW:units = "degrees_north" ;
#               ROW:long_name = "latitude" ;
#       float COL(COL) ;
#               COL:units = "degrees_east" ;
#               COL:long_name = "longitude" ;
#
# // global attributes:
#               :title = "daily GFED N2O fractions for 2008" ;
#               :creation_date = "Wed Apr  3 21:25:53 EDT 2013" ;
#               :source_file = "assembled from data from ftp://gfed3:dailyandhourly@zea.ess.uci.edu/GFEDv3.1/daily/2008.tar.gz" ;
#               :Conventions = "see https://bitbucket.org/epa_n2o_project_team/gfed-3.1_global_to_aqmeii-na" ;
#               :YEAR = 2008 ;
#               :HISTORY = "see https://bitbucket.org/epa_n2o_project_team/gfed-3.1_global_to_aqmeii-na" ;
# }
} # end function conv_dailies

### Call NCL helper to convert the 12 {raw, as downloaded from GFED} 3hourly-fraction netCDF files
### to a single netCDF file with TSTEP=12 and LAY=8 (== 24/3), for ease of regridding as a single RasterBrick

### Depends on function get_conv_3hourlies above.
### Assumes `ncl` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to NCL
### for now: just use envvars :-(
function conv_3hourlies {
# eval "rm ${GFED_GLOBAL_3HOURLY_FRAC_CONV_FP}"
# ncl # bail to NCL and copy script lines, or ...
  if [[ ! -r "${GFED_GLOBAL_3HOURLY_FRAC_CONV_FP}" ]] ; then
    for CMD in \
      "ncl -n ${CONV_3HOURLIES_FP}" \
      "ncdump -h ${GFED_GLOBAL_3HOURLY_FRAC_CONV_FP}" \
    ; do
    cat <<EOM

About to run command='${CMD}'. WARNING: may seem to hang while processing!

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 1
      fi
    done
  fi
# `ncdump -h` ->
# netcdf GFED-3.1_2008_N2O_3hourly_fractions {
# dimensions:
#       TSTEP = 12 ;
#       LAY = 8 ;
#       ROW = 360 ;
#       COL = 720 ;
# variables:
#       float Fraction_of_Emissions(TSTEP, LAY, ROW, COL) ;
#               Fraction_of_Emissions:units = "unitless" ;
#               Fraction_of_Emissions:long_name = "3hourly fraction of monthly N2O emission" ;
#               Fraction_of_Emissions:_FillValue = 9.96921e+36f ;
#       int TSTEP(TSTEP) ;
#               TSTEP:long_name = "index of month in 2008" ;
#               TSTEP:units = "month" ;
#       int LAY(LAY) ;
#               LAY:units = "4-digit hour" ;
#               LAY:long_name = "time of day starting 3-hour-long period" ;
#       float ROW(ROW) ;
#               ROW:units = "degrees_north" ;
#               ROW:long_name = "latitude" ;
#       float COL(COL) ;
#               COL:units = "degrees_east" ;
#               COL:long_name = "longitude" ;
#
# // global attributes:
#               :title = "3hourly GFED N2O fractions for 2008" ;
#               :creation_date = "Wed Apr  3 21:48:37 EDT 2013" ;
#               :source_file = "assembled from data from ftp://gfed3:dailyandhourly@zea.ess.uci.edu/GFEDv3.1/3-hourly/2008.tar.gz" ;
#               :Conventions = "see https://bitbucket.org/epa_n2o_project_team/gfed-3.1_global_to_aqmeii-na" ;
#               :YEAR = 2008 ;
#               :HISTORY = "see https://bitbucket.org/epa_n2o_project_team/gfed-3.1_global_to_aqmeii-na" ;
# }
} # end function conv_3hourlies

### Get AQMEII-NA gridcell map scale factors (MSFs).
function get_MSFs {
  if [[ -z "${AQMEIINA_MSFs_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: AQMEIINA_MSFs_FP not defined"
    exit 1
  fi
  if [[ ! -r "${AQMEIINA_MSFs_FP}" ]] ; then
    if [[ ! -r "${MSF_GZ_FP}" ]] ; then
      for CMD in \
        "${WGET_TO_FILE} ${AQMEIINA_MSFs_GZ_FP} ${AQMEIINA_MSFs_GZ_URI}" \
      ; do
        echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
        eval "${CMD}" # comment this out for NOPing, e.g., to `source`
        if [[ $? -ne 0 ]] ; then
          echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
          exit 1
        fi
      done
    fi
    if [[ -r "${AQMEIINA_MSFs_GZ_FP}" ]] ; then
      for CMD in \
        "gunzip ${AQMEIINA_MSFs_GZ_FP}" \
      ; do
        echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
        eval "${CMD}" # comment this out for NOPing, e.g., to `source`
        if [[ $? -ne 0 ]] ; then
          echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
          exit 1
        fi
      done
    fi
  fi
  if [[ ! -r "${AQMEIINA_MSFs_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot read AQMEIINA_MSFs_FP=='${MSF_FP}'"
    exit 1
  fi
} # end function get_MSFs

### Convert GFED inputs:
### * monthly emissions: from 12 *.txt in flux rate (gN2O/m^2/mo) to a single netCDF "brick" in molar rate (molN2O/mo)
### * daily fractions: from 366 *.nc to one
### * 3-hourly fractions: from 12 *.nc to one
function convert_inputs {
  for CMD in \
    'get_input_areas' \
    'conv_monthlies' \
    'conv_dailies' \
    'conv_3hourlies' \
  ; do
    echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
      exit 1
    fi
  done
} # end function convert_inputs

### Depends on function get_vis_regrid_vis above.
### Assumes `Rscript` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to R
### for now: just use envvars :-(
function vis_regrid_vis {
  if [[ -z "${GFED_REGRID_MONTHLY_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: GFED_REGRID_MONTHLY_FP not defined"
    exit 1
  fi
  if [[ -z "${GFED_REGRID_DAILY_FRAC_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: GFED_REGRID_DAILY_FRAC_FP not defined"
    exit 1
  fi
  if [[ -z "${GFED_REGRID_3HOURLY_FRAC_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: GFED_REGRID_3HOURLY_FRAC_FP not defined"
    exit 1
  fi

# eval "rm ${GFED_REGRID_MONTHLY_FP}"
# R # bail to R console and copy script lines, or ...
  if [[ -r "${GFED_REGRID_MONTHLY_FP}" &&
        -r "${GFED_REGRID_DAILY_FRAC_FP}" &&
        -r "${GFED_REGRID_3HOURLY_FRAC_FP}" ]] ; then
    echo -e "${THIS_FN}::${FUNCNAME[0]}: skipping regrid: monthly, daily, and 3-hourly regrid files all readable"
  else # make them: but since R::raster can't seem to regrid 4D,
    # use NCL to reassemble the separately-regridded 3hourlies
# ncl
    for CMD in \
      "Rscript ${VIS_REGRID_VIS_FP}" \
      "ncl -n ${FIX_3HOURLIES_FP}" \
    ; do
    cat <<EOM

About to run command='${CMD}'. WARNING: may seem to hang while processing!

EOM
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 1
      fi
    done
    for CMD in \
      "${PDF_VIEWER} ${GFED_GLOBAL_MONTHLY_CONV_PDF_FP} &" \
      "ncdump -h ${GFED_REGRID_MONTHLY_FP}" \
      "${PDF_VIEWER} ${GFED_REGRID_MONTHLY_PDF_FP} &" \
      "ncdump -h ${GFED_REGRID_DAILY_FRAC_FP}" \
      "ncdump -h ${GFED_REGRID_3HOURLY_FRAC_FP}" \
    ; do
      echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
      eval "${CMD}" # comment this out for NOPing, e.g., to `source`
      if [[ $? -ne 0 ]] ; then
        echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
        exit 1
      fi
    done
  fi
} # end function vis_regrid_vis

### Call NCL helper to check the presence and orientation of coordinate variables in the outputs of

### * conv_monthlies
### * conv_dailies
### * conv_3hourlies
### * vis_regrid_vis
### * get_template

### Depends on function get_check_coord_vars above.
### Assumes `ncl` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to NCL
### for now: just use envvars :-(
function check_coord_vars {
# ncl # bail to NCL and copy script lines, or ...
  for CMD in \
    "ncl -n ${CHECK_COORD_VARS_FP}" \
  ; do
    echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
      exit 1
    fi
  done
} # end function check_coord_vars

### Call NCL helper to

### Depends on function get_make_hourlies above.
### Assumes `ncl` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to NCL
### for now: just use envvars :-(
function make_hourlies {
# rm ./emis_mole_N2O_*.ncf*
# ncl # bail to NCL and copy script lines, or ...
  for CMD in \
    "ncl -n ${MAKE_HOURLIES_FP}" \
  ; do
    echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
      exit 1
    fi
  done
} # end function make_hourlies

### Check conservation of mass from GFED global monthlies to
### regridded (by me) monthlies to regridded hourlies, using input units.
### Depends on function get_check_conservation above.
### Assumes `ncl` is in PATH: see bash_utilities.sh
### TODO: pass commandline args to NCL
### for now: just use envvars :-(
function check_conservation {
# ncl # bail to NCL and copy script lines, or ...
  for CMD in \
    "ncl -n ${CHECK_CONSERVATION_FP}" \
  ; do
    echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
      exit 1
    fi
  done
} # end function check_conservation

function teardown {
#  echo -e 'TODO!'
  for CMD in \
    "ls -alt ${WORK_DIR}/*.${NETCDF_EXT_NCL} | head" \
    "ls -alt ${WORK_DIR}/*.${NETCDF_EXT_CMAQ} | head" \
    "ls -alt ${WORK_DIR}/*.pdf | head" \
  ; do
    echo -e "\n$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}\n"
    eval "${CMD}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "$ ${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed or not found\n"
      exit 1
    fi
  done
} # end function teardown

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

#  'setup' \              # paths, helpers, etc
#  'download_GFED' \      # get raw inputs from GFED website(s)
#  'check_raw_fracs' \    # check fraction inputs from GFED website(s)
#  'convert_inputs' \     # convert monthly emissions and daily and 3-hourly fractions
#  'vis_regrid_vis' \     # regrid global -> AQMEII, plotting before and after
#  'check_coord_vars' \   # presence and orientation of output coordvars
#  'make_hourlies' \      # create our real output: hourly emissions for CMAQ
#  'check_conservation' \ # of mass from inputs to outputs
#  'teardown' \
# should always
# * begin with `setup` to do `module add`
# * end with `teardown` for tidy and testing (e.g., plot display)
for CMD in \
  'setup' \
  'convert_inputs' \
  'vis_regrid_vis' \
  'make_hourlies' \
  'check_conservation' \
  'teardown' \
; do
  echo -e "\n$ ${THIS_FN}::${CMD}"
  eval "${CMD}" # comment this out for NOPing, e.g., to `source`
  if [[ $? -ne 0 ]] ; then
    echo -e "$ ${THIS_FN}::main loop::${CMD}: ERROR: failed or not found\n"
    exit 1
  fi
done

# ----------------------------------------------------------------------
# debugging
# ----------------------------------------------------------------------
