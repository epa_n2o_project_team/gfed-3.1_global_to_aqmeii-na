;!/usr/bin/env ncl ; requires version >= ???
;;; ----------------------------------------------------------------------
;;; Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

;;; This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

;;; * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

;;; * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

;;; This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
;;; ----------------------------------------------------------------------

; Convert the {raw, as downloaded from GFED} 12 monthly emissions native.txt files
; * from native .txt format to netCDF
; * to a single netCDF file with 12 TSTEP, for ease of regridding as a single RasterBrick
; * from non-regriddable flux-rate units (explicit timestep) to molar-mass (implicit timestep): i.e., multiply by gridcell areas

;----------------------------------------------------------------------
; libraries
;----------------------------------------------------------------------

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"    ; all built-ins?
;load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl" ; for readAsciiTable, stat_dispersion

load "$FILEPATH_FUNCS_FP"  ; for get_*ly_fp

;----------------------------------------------------------------------
; functions
;----------------------------------------------------------------------
  
;----------------------------------------------------------------------
; code
;----------------------------------------------------------------------

begin ; skip if copy/paste-ing to console

;----------------------------------------------------------------------
; constants
;----------------------------------------------------------------------

  ;;; model constants
  this_fn = getenv("CONV_MONTHLIES_FN") ; for debugging
  model_year = stringtoint(getenv("MODEL_YEAR"))
  molar_mass_N2O = stringtofloat(getenv("MOLAR_MASS_N2O"))
  ; since we're dealing with numerical math:
  zero_max = stringtofloat(getenv("ZERO_MAX"))
  one_min = stringtofloat(getenv("ONE_MIN"))

  ;;; areas (input)
  areas_fp = getenv("GFED_AREAS_GLOBAL_FP")
  areas_datavar_name = getenv("GFED_AREAS_GLOBAL_DATAVAR_NAME")

  ;;; monthly emission inputs
;  monthly_template_string = getenv("GFED_GLOBAL_MONTHLY_TEMPLATE_STRING")
  template_string = getenv("TEMPLATE_STRING")
  monthly_fp_template = getenv("GFED_GLOBAL_MONTHLY_FP_TEMPLATE")
  monthly_n_col = stringtoint(getenv("GFED_GLOBAL_MONTHLY_TXT_COLS"))
  monthly_n_row = stringtoint(getenv("GFED_GLOBAL_MONTHLY_TXT_ROWS"))
  monthly_n_head = stringtoint(getenv("GFED_GLOBAL_MONTHLY_TXT_HEADERS"))
  monthly_n_foot = stringtoint(getenv("GFED_GLOBAL_MONTHLY_TXT_FOOTERS"))
  ; numbers of header and footer lines to ignore in `readAsciiTable`
  monthly_n_ignore = (/ monthly_n_head, monthly_n_foot /)
  
  ;;; annual emission output
  out_fp = getenv("GFED_GLOBAL_MONTHLY_CONV_FP")
  out_attr_creation_date = systemfunc("date") ; the canonical way
  out_attr_history = getenv("GFED_GLOBAL_MONTHLY_CONV_HISTORY")
  out_attr_source_file = getenv("GFED_GLOBAL_MONTHLY_CONV_SOURCE_FILE")
  out_attr_title = getenv("GFED_GLOBAL_MONTHLY_CONV_TITLE")

  out_datavar_name = getenv("GFED_GLOBAL_MONTHLY_CONV_DATAVAR_NAME")
  out_datavar_attr_units = getenv("GFED_GLOBAL_MONTHLY_CONV_DATAVAR_UNITS")
  out_datavar_attr_long_name = getenv("GFED_GLOBAL_MONTHLY_CONV_DATAVAR_LONG_NAME")
  ; out_datavar dims (TSTEP, LAY, ROW, COL):
  ;   TSTEP = 12 ;
  ;   LAY = 1 ;
  ;   ROW = 360 ;
  ;   COL = 720 ;
  out_n_tstep = stringtoint(getenv("GFED_GLOBAL_MONTHLY_CONV_N_TSTEP"))
  out_n_layer = stringtoint(getenv("GFED_GLOBAL_MONTHLY_CONV_N_LAYER"))
  out_n_row = stringtoint(getenv("GFED_GLOBAL_MONTHLY_CONV_N_ROW"))
  out_n_col = stringtoint(getenv("GFED_GLOBAL_MONTHLY_CONV_N_COL"))
  out_datavar_dim_sizes = \
    (/out_n_tstep, out_n_layer, out_n_row, out_n_col /)

  out_dim_time_name = getenv("GFED_GLOBAL_MONTHLY_CONV_DIM_TIME_NAME")
  out_dim_time_attr_units = getenv("GFED_GLOBAL_MONTHLY_CONV_DIM_TIME_UNITS")
  out_dim_time_attr_long_name = getenv("GFED_GLOBAL_MONTHLY_CONV_DIM_TIME_LONG_NAME")

  out_dim_layer_name = getenv("GFED_GLOBAL_MONTHLY_CONV_DIM_LAYER_NAME")

  out_dim_x_name = getenv("GFED_GLOBAL_MONTHLY_CONV_DIM_X_NAME")
  out_dim_x_attr_units = getenv("GFED_GLOBAL_MONTHLY_CONV_DIM_X_UNITS")
  out_dim_x_attr_long_name = getenv("GFED_GLOBAL_MONTHLY_CONV_DIM_X_LONG_NAME")

  out_dim_y_name = getenv("GFED_GLOBAL_MONTHLY_CONV_DIM_Y_NAME")
  out_dim_y_attr_units = getenv("GFED_GLOBAL_MONTHLY_CONV_DIM_Y_UNITS")
  out_dim_y_attr_long_name = getenv("GFED_GLOBAL_MONTHLY_CONV_DIM_Y_LONG_NAME")

;----------------------------------------------------------------------
; payload
;----------------------------------------------------------------------

; Convert the {raw, as downloaded from GFED} 12 monthly emissions native.txt files
; * from native .txt format to netCDF
; * to a single netCDF file with 12 TSTEP, for ease of regridding as a single RasterBrick
; * from flux-rate units (explicit timestep) to molar-mass (implicit timestep): i.e., multiply by gridcell areas

;----------------------------------------------------------------------
; setup annual output
;----------------------------------------------------------------------

  ;;; create output data file
  out_fh = addfile(out_fp, "c") ; use workaround if `source`ing driver

  ;; define global attributes
  out_fh_att = True ; assign file/global attributes
  out_fh_att@HISTORY = out_attr_history
  out_fh_att@YEAR = model_year
  out_fh_att@Conventions = out_attr_history
  out_fh_att@source_file = out_attr_source_file
  out_fh_att@creation_date = out_attr_creation_date
  out_fh_att@title = out_attr_title

  ;; setup file data variable (datavar) and its coordinate variables
  ;; will fill data inside input-read loop

  temp_var = new(out_datavar_dim_sizes, float)
  ; add datavar attributes
  temp_var@long_name = out_datavar_attr_long_name
  temp_var@units = out_datavar_attr_units
  ; name dimensions, provide coordinates: we want out_datavar dims
  ; (TSTEP, LAY, ROW, COL):
  ;   TSTEP = 12 ;
  ;   LAY = 1 ;
  ;   ROW = 360 ;
  ;   COL = 720 ;

  temp_var!0 = out_dim_time_name
  out_dim_time_attr_units = "month" ; getenv("")
  out_dim_time_attr_long_name = "index of month in 2008" ; getenv("")

  temp_var!1 = out_dim_layer_name

  temp_var!2 = out_dim_y_name
  out_dim_y_attr_units = "degrees_north" ; getenv("")
  out_dim_y_attr_long_name = "latitude" ; getenv("")

  temp_var!3 = out_dim_x_name
  out_dim_x_attr_units = "longitude" ; getenv("")
  out_dim_x_attr_long_name = "degrees_east" ; getenv("")

;  ; start debugging-----------------------------------------------------
;  printVarSummary(temp_var)
;  ;   end debugging-----------------------------------------------------

;----------------------------------------------------------------------
; load areas
;----------------------------------------------------------------------

  areas_fh = addfile(areas_fp, "r")
  areas_datavar = areas_fh->$areas_datavar_name$
  areas_mx = areas_datavar(:,:) ; ensure stripped of metadata?

;  ; start debugging-----------------------------------------------------
;  print("areas from '" + areas_fp + "'==")
;  printVarSummary(areas_datavar)
;  ;   end debugging-----------------------------------------------------

;----------------------------------------------------------------------
; iterate monthly inputs
;----------------------------------------------------------------------

  do n_month = 1, 12
    i_month = n_month - 1
    monthly_fp = get_monthly_fp(\
      monthly_fp_template, template_string, model_year, n_month)

    ;;; get input data in flux-rate units (gN2O/m^2/mo, explicit timestep)
    ; waaay faster on terrae than tlrPanP5
    monthly_in_mx = \ ;
      readAsciiTable(monthly_fp, monthly_n_col, "float", monthly_n_ignore) 

    ;;; Flip data latitudinally, i.e., around equator.
    ;;; Why is this necessary? not sure, but it is, per
    ;;; * thread beginning @ https://stat.ethz.ch/pipermail/r-help/2012-July/319255.html
    ;;; * subsequent plotting
    ;;; I dunno how to do that, but fortunately that's same as reversing:
    ;;; dimensions are (lat, lon) (aka (ROW, COL)), so ...
    monthly_out_mx = monthly_in_mx(::-1, :)

;    ; start debugging-----------------------------------------------------
;    print("raw data from '" + monthly_fp + "'==")
;    printVarSummary(monthly_in_mx)
;
;    monthly_zeros = num(monthly_in_mx .lt. zero_max)
;    monthly_stats = \
;      stat_dispersion(monthly_in_mx, False) ; False == don't stdout
;    print(\
;      str_get_nl() + \
;      "|cells|=" + monthly_stats(18)                   + str_get_nl() + \
;      "|obs|  =" + monthly_stats(19)                   + str_get_nl() + \
;      "min    =" + sprintf("%7.3e", monthly_stats(2))  + str_get_nl() + \
;      "q1     =" + sprintf("%7.3e", monthly_stats(6))  + str_get_nl() + \
;      "med    =" + sprintf("%7.3e", monthly_stats(8))  + str_get_nl() + \
;      "mean   =" + sprintf("%7.3e", monthly_stats(0))  + str_get_nl() + \
;      "q3     =" + sprintf("%7.3e", monthly_stats(10)) + str_get_nl() + \
;      "max    =" + sprintf("%7.3e", monthly_stats(14)) + str_get_nl() + \
;      "|cells < "+zero_max+"|=" + sprinti("%6i", monthly_zeros) + str_get_nl() + \
;      str_get_nl() )
;    ;   end debugging-----------------------------------------------------

    ;;; Convert flux-rate to molar mass:
    ;;; from GFED-supplied monthly (units==gN2O/m^2/mo per http://www.globalfiredata.org/Data/index.html )
    ;;; to the more-regriddable and CMAQ-friendly molN2O/mo :

    ;;; gN2O     m^2   molN2O
    ;;; ------ * --- * ------
    ;;; m^2 mo         gN2O

    ; '*' is not linear-algebra-style multiplication (for which use '#')
    monthly_out_mx = (monthly_out_mx * areas_mx) / molar_mass_N2O

;    ; start debugging-----------------------------------------------------
;    print("molarized data from '" + monthly_fp + "'==")
;    printVarSummary(monthly_out_mx)
;    monthly_zeros = num(monthly_out_mx .lt. zero_max)
;    monthly_stats = \
;      stat_dispersion(monthly_out_mx, False) ; False == don't stdout
;    print(\
;      str_get_nl() + \
;      "|cells|=" + monthly_stats(18)                   + str_get_nl() + \
;      "|obs|  =" + monthly_stats(19)                   + str_get_nl() + \
;      "min    =" + sprintf("%7.3e", monthly_stats(2))  + str_get_nl() + \
;      "q1     =" + sprintf("%7.3e", monthly_stats(6))  + str_get_nl() + \
;      "med    =" + sprintf("%7.3e", monthly_stats(8))  + str_get_nl() + \
;      "mean   =" + sprintf("%7.3e", monthly_stats(0))  + str_get_nl() + \
;      "q3     =" + sprintf("%7.3e", monthly_stats(10)) + str_get_nl() + \
;      "max    =" + sprintf("%7.3e", monthly_stats(14)) + str_get_nl() + \
;      "|cells < "+zero_max+"|=" + sprinti("%6i", monthly_zeros) + str_get_nl() + \
;      str_get_nl() )
;    ;   end debugging-----------------------------------------------------

    ;;; write data to output
    temp_var(i_month, 0, \ ; all one layer: one species @ surface
             :,:) = (/ monthly_out_mx /)
    ; note: if one does instead
;             :,:) = monthly_out_mx
    ; NCL will delete dimension names from temp_var!

  end do ; n_month

  ;;; finish output data file
  ;; write datavar
  out_fh->$out_datavar_name$ = temp_var

  ;; write coordinate attributes: must do this after writing datavar?
  ; temp_var!0 = out_dim_time_name
  ; can't define attributes without first defining variable
;  out_fh->$out_dim_time_name$ = (/ ispan(1, 12, 1) /) ; index numbers of the months
;  out_fh->$out_dim_time_name$ = (/ toint(temp_var(:,0,0,0)) /)
;  out_fh->$out_dim_time_name$ = (/ toint(out_fh->$out_datavar_name$(:,0,0,0)) /)
;  out_fh->$out_dim_time_name$@units = out_dim_time_attr_units
;  out_fh->$out_dim_time_name$@long_name = out_dim_time_attr_long_name
  ; ARRGGGHHHH! above also fails: writes
;  dimensions:
;        TSTEP = 12 ;
;        LAY = 1 ;
;        ROW = 360 ;
;        COL = 720 ;
;        ncl4 = 12 ;
;variables:
;        float N2O(TSTEP, LAY, ROW, COL) ;
;                N2O:units = "molN20/mo" ;
;                N2O:long_name = "N2O emissions" ;
;                N2O:_FillValue = 9.96921e+36f ;
;        int TSTEP(ncl4) ;
;                TSTEP:units = "month" ;
;                TSTEP:long_name = "index of month in 2008" ;
  
  ; temp_var!2 = out_dim_x_name
;  ; start debugging-----------------------------------------------------
;  printVarSummary(areas_fh->$out_dim_x_name$)
;;  printVarSummary(out_fh->$out_dim_x_name$) ; no, it's not defined yet
;  printVarSummary(out_fh->$out_datavar_name$)
;  print(systemfunc("ncdump -h " + out_fp))
;  ;   end debugging-----------------------------------------------------
  out_fh->$out_dim_x_name$ = areas_fh->$out_dim_x_name$
  ; these will come from areas' var
;  out_fh->$out_dim_x_name$@units = out_dim_x_attr_units
;  out_fh->$out_dim_x_name$@long_name = out_dim_x_attr_long_name

  ; temp_var!3 = out_dim_y_name
  out_fh->$out_dim_y_name$ = areas_fh->$out_dim_y_name$
  ; these will come from areas' var
;  out_fh->$out_dim_y_name$@units = out_dim_y_attr_units
;  out_fh->$out_dim_y_name$@long_name = out_dim_y_attr_long_name
    
  ;; write global attributes: can (must?) do this after writing datavar
  fileattdef(out_fh, out_fh_att)

; ; start debugging-----------------------------------------------------
;   out_datavar = out_fh->$out_datavar_name$
;   print("molarized data from '" + out_fp + "'==")
;   printVarSummary(out_datavar)
; ; print(systemfunc("ncdump -h " + out_fp))
; 
; ;----------------------------------------------------------------------
; ; iterate months in output
; ;----------------------------------------------------------------------
; 
;   do n_month = 1, 12
;     i_month = n_month - 1
;     monthly_out_mx = out_datavar(i_month,0,:,:)
;     monthly_zeros = num(monthly_out_mx .lt. zero_max)
;     monthly_stats = \
;       stat_dispersion(monthly_out_mx, False) ; False == don't stdout
;     print(\
;       str_get_nl() + \
;       "molarized output for month=" + n_month + ":"    + str_get_nl() + \
;       "|cells|=" + monthly_stats(18)                   + str_get_nl() + \
;       "|obs|  =" + monthly_stats(19)                   + str_get_nl() + \
;       "min    =" + sprintf("%7.3e", monthly_stats(2))  + str_get_nl() + \
;       "q1     =" + sprintf("%7.3e", monthly_stats(6))  + str_get_nl() + \
;       "med    =" + sprintf("%7.3e", monthly_stats(8))  + str_get_nl() + \
;       "mean   =" + sprintf("%7.3e", monthly_stats(0))  + str_get_nl() + \
;       "q3     =" + sprintf("%7.3e", monthly_stats(10)) + str_get_nl() + \
;       "max    =" + sprintf("%7.3e", monthly_stats(14)) + str_get_nl() + \
;       "|cells < "+zero_max+"|=" + sprinti("%6i", monthly_zeros) + str_get_nl() + \
;       str_get_nl() )
; 
;   end do ; n_month
; ;   end debugging-----------------------------------------------------

;  ; start debugging-----------------------------------------------------
;  ; TODO: do this in driver
;  print(systemfunc("ncdump -h " + out_fp))
;; netcdf GFED-3.1_2008_N2O {
;; dimensions:
;;       TSTEP = 12 ;
;;       LAY = 1 ;
;;       ROW = 360 ;
;;       COL = 720 ;
;; variables:
;;       float N2O(TSTEP, LAY, ROW, COL) ;
;;               N2O:units = "molN20/mo" ;
;;               N2O:long_name = "N2O emissions" ;
;;               N2O:_FillValue = 9.96921e+36f ;
;;       float COL(COL) ;
;;               COL:units = "degrees_east" ;
;;               COL:long_name = "longitude" ;
;;               COL:standard_name = "longitude" ;
;;       float ROW(ROW) ;
;;               ROW:units = "degrees_north" ;
;;               ROW:long_name = "latitude" ;
;;               ROW:standard_name = "latitude" ;
;
;; // global attributes:
;;               :title = "monthly GFED N2O emissions for 2008" ;
;;               :creation_date = "Wed Mar 27 23:10:22 EDT 2013" ;
;;               :source_file = "assembled from data from http://www.falw.vu/~gwerf/GFED/GFED3/emissions/GFED3.1_N2O.zip" ;
;;               :Conventions = "see https://bitbucket.org/epa_n2o_project_team/gfed-3.1_global_to_aqmeii-na" ;
;;               :YEAR = 2008 ;
;;               :HISTORY = "see https://bitbucket.org/epa_n2o_project_team/gfed-3.1_global_to_aqmeii-na" ;
;; }
;  ;   end debugging-----------------------------------------------------

end ; convert_monthlies.ncl
