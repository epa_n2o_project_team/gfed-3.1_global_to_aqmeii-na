#!/usr/bin/env bash
### ----------------------------------------------------------------------
### Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

### This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

### * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

### * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

### This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
### ----------------------------------------------------------------------

# This script clones and runs code either from
# * the `git` repository @
# https://tlroche@bitbucket.org/tlroche/gfed-3.1_global_to_aqmeii-na.git
#   You probably wanna check its README before running this.

# * the current folder (for code you haven't committed).

# If you decide to run this, you should first

# * un/comment var=ACCESS to reflect your available/desired file-transfer protocol
# * if using ACCESS='file', edit FILE_LIST below
# * edit var=REPO_ROOT to state where to create your repo clone
# * remember to enable netCDF-4 on your system, e.g.
# > module add netcdf-4.1.2 # on terrae (a cluster on which I work)

# and run this like
# START="$(date)" ; echo -e "START=${START}" ; rm -fr /tmp/gfed-3.1_global_to_aqmeii-na/ ; ./uber_driver.sh ; END="$(date)" ; echo -e "  END=${END}" ; echo -e "START=${START}"

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

THIS=$0
THIS_FN="$(basename ${THIS})"

REPO_ROOT='/tmp' # or location of your choice: repo/workspace created in subdir
PROJECT_NAME='gfed-3.1_global_to_aqmeii-na' # TODO: get from commandline
REPO_DIR="${REPO_ROOT}/${PROJECT_NAME}"
# what actually does the work
DRIVER_FN='GFED_driver.sh'                  # TODO: get from commandline

### following are "booleans": comment out or change value if !true
# # Does our platform allow git/ssh? (e.g., terrae does not)
# CAN_GIT_SSH='true'
# # Does our platform have HTTP certificates? (e.g., terrae does not)
# HAVE_HTTP_CERTS='true'
# if latter is false, git must not check for certificates
if [[ -z "${HAVE_HTTP_CERTS}" || "${HAVE_HTTP_CERTS}" != 'true' ]] ; then
  # set for ${DRIVER_FN}
  export GIT_CLONE_PREFIX='env GIT_SSL_NO_VERIFY=true'
else
  export GIT_CLONE_PREFIX=''
fi

### modes of access to sources (see setup_sources): either copy files or get from repo
# ACCESS='file'       # copy code in current directory
# ACCESS=='file' uses no repository
# ACCESS='git/ssh'    # outgoing ssh works (preferred if available)
# REPO_URI_GIT="git@bitbucket.org:tlroche/${PROJECT_NAME}.git"
# ACCESS='http+cert'  # outgoing ssh blocked, but you have certificates
REPO_URI_HTTP="https://tlroche@bitbucket.org/tlroche/${PROJECT_NAME}.git"
ACCESS='http-cert'  # (terrae) outgoing ssh blocked, no certs installed
# also uses REPO_URI_HTTP

# Workaround for when GFED, aka zea.ess.uci.edu, is down (i.e., not available for downloading)
# Comment out as needed. TODO: get from commandline
GFED_DOWN='true'
# note following literals are copied from driver. TODO: get from common properties!
GFED_GLOBAL_DAILY_FRAC_GZ_FAKE_FN='daily.2008.tar.gz'
GFED_GLOBAL_3HOURLY_FRAC_GZ_FAKE_FN='3hourly.2008.tar.gz'

# if [[ "${ACCESS}" == 'file' ]], you'll need
FILE_LIST="./${DRIVER_FN} \
           ./check_conservation.ncl \
           ./check_coord_vars.ncl \
           ./check_raw_fractions.ncl \
           ./convert_3hourlies.ncl \
           ./convert_dailies.ncl \
           ./convert_monthlies.ncl \
           ./fix_3hourlies.ncl \
           ./make_hourlies.ncl \
           ./vis_regrid_vis.r "
# TODO: get members(${FILE_LIST}) from ${DRIVER_FN}

if [[ "${GFED_DOWN}" == 'true' ]] ; then
  # note following 
  FILE_LIST="${FILE_LIST} \
             ./${GFED_GLOBAL_DAILY_FRAC_GZ_FAKE_FN} \
             ./${GFED_GLOBAL_3HOURLY_FRAC_GZ_FAKE_FN}"
fi

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

function setup_sources {
  if [[ "${ACCESS}" == 'file' ]] ; then
    if [[ ! -x "./${DRIVER_FN}" ]] ; then
      echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: driver=='${DRIVER_FN}' not executable (with ACCESS==${ACCESS})"
      exit 1
    else
      for CMD in \
        "mkdir -p ${REPO_DIR}" \
        "cp ${FILE_LIST} ${REPO_DIR}/ " \
      ; do
        echo -e "$ ${CMD}"
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
            echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed (with ACCESS==${ACCESS})\n"
            exit 2
        fi
      done
    fi

  else # ACCESS != 'file', i.e., from repo

    if   [[ "${ACCESS}" == 'git/ssh' ]] ; then
      if [[ -n "${CAN_GIT_SSH}" && "${CAN_GIT_SSH}" == 'true' ]] ; then
        REPO_URI="${REPO_URI_GIT}"
      else
        echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot use access==git/ssh on this platform\n"
        exit 3
      fi
    elif [[ "${ACCESS}" == 'http+cert' ]] ; then
      if [[ -n "${HAVE_HTTP_CERTS}" && "${HAVE_HTTP_CERTS}" == 'true' ]] ; then
        REPO_URI="${REPO_URI_HTTP}"
      else
        echo -e "${THIS_FN}::${FUNCNAME[0]}: ERROR: cannot use access==http+cert on this platform\n"
        exit 4
      fi
    elif [[ "${ACCESS}" == 'http-cert' ]] ; then
      REPO_URI="${REPO_URI_HTTP}"
    fi

    for CMD in \
      "pushd ${REPO_ROOT}" \
      "${GIT_CLONE_PREFIX} git clone ${REPO_URI}" \
      "popd" \
    ; do
      echo -e "$ ${CMD}"
      eval "${CMD}"
      if [[ $? -ne 0 ]] ; then
          echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed (with ACCESS==${ACCESS})\n"
          exit 5
      fi
    done

    if [[ -n "${GFED_DOWN}" && "${GFED_DOWN}" == 'true' ]] ; then
      for CMD in \
        "cp ./${GFED_GLOBAL_DAILY_FRAC_GZ_FAKE_FN} ${REPO_DIR}/" \
        "cp ./${GFED_GLOBAL_3HOURLY_FRAC_GZ_FAKE_FN} ${REPO_DIR}/" \
      ; do
        echo -e "$ ${CMD}"
        eval "${CMD}"
        if [[ $? -ne 0 ]] ; then
            echo -e "${THIS_FN}::${FUNCNAME[0]}::${CMD}: ERROR: failed (with ACCESS==${ACCESS})\n"
            exit 5
        fi
      done
    fi # end testing GFED_DOWN
  fi # end testing ACCESS
} # end function setup_sources

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

if [[ -z "${REPO_DIR}" ]] ; then
  echo -e "${THIS_FN}:ERROR: REPO_DIR not defined"
  exit 6
fi

if [[ -d "${REPO_DIR}" ]] ; then
  echo -e "${THIS_FN}:ERROR? repo dir='${REPO_DIR}' exists: move or delete it before running this script"
  exit 7
fi

if [[ -z "${ACCESS}" ]] ; then
  echo -e "${THIS_FN}: ERROR: ACCESS not defined: did you uncomment one of the choices above?"
  exit 8
fi

#   'setup_resources' \
for CMD in \
  'setup_sources' \
  "pushd ${REPO_DIR}" \
  "./${DRIVER_FN}" \
; do
#   "ls -alhR ${REPO_DIR}" \
  echo -e "\n$ ${THIS_FN}::main loop::${CMD}\n"
  eval "${CMD}" # comment this out for NOPing, e.g., to `source`
  if [[ $? -ne 0 ]] ; then
    echo -e "${THIS_FN}::main loop::${CMD}: ERROR: failed or not found\n"
    exit 9
  fi
done
