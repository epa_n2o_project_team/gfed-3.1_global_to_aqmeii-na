*(part of the [AQMEII-NA_N2O][AQMEII-NA_N2O wiki home] family of projects)*

**table of contents**

[TOC]

# open-source notice

Copyright 2013, 2014 Tom Roche <Tom_Roche@pobox.com>

This project's content is free software: you can redistribute it and/or modify it provided that you do so as follows:

* under the terms of the [GNU Affero General Public License][GNU Affero General Public License HTML @ GNU] as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
* preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

This project's content is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the [GNU Affero General Public License][GNU Affero General Public License local text] for more details.

![distributed under the GNU Affero General Public License](../../downloads/Affero_badge__agplv3-155x51.png)

[GNU Affero General Public License local text]: ./COPYING
[GNU Affero General Public License HTML @ GNU]: https://www.gnu.org/licenses/agpl.html

# description

Uses [bash][bash @ wikipedia] to drive [NCL][NCL @ wikipedia] and [R][R @ wikipedia] code to

1. process [GFED-3.1][GFED-3.1 overview] N2O emission inventories for the model year=2008:
    1. retrieve [GFED inputs][GFED-3.1 data], gridded globally @ resolution=`(0.5°x0.5°)`, units=`gN2O/m^2/mo`
        1. monthly emissions: extracted from [here][GFED-3.1 N2O archive], in GFED native text format
        1. daily fractions: retrieved from [here][GFED-3.1 N2O 2008 daily fractions archive] (may only be visible from FTP client) as compressed netCDF
        1. 3-hourly fractions: retrieved from [here][GFED-3.1 N2O 2008 daily fractions archive] (may only be visible from FTP client) as compressed netCDF
    1. convert individual monthly `*.txt` files to a [single annual netCDF][converted monthly GFED-3.1 netCDF] file with
        * 12 timesteps
        * units=`molN2O/mo`, using computed [gridcell areas][gridcell areas netCDF]. Mass rates are more regriddable than (the GFED-native) flux rates (see above).
    1. convert individual daily-fraction netCDF files to a single annual netCDF with 366 timesteps
    1. convert individual 3-hourly-fraction netCDF files to a single annual netCDF with 12 timesteps (3-hourly-fractions are delivered by month)
1. 2D-regrid the annualized netCDF "bricks" from global/unprojected to a projected subdomain ([AQMEII-NA][]).
    1. [visualize the global monthlies][visualization of global monthlies]
    1. 2D-regrid monthlies, dailies, and 3-hourlies
    1. [visualize the AQMEII-NA monthlies][visualization of AQMEII-NA monthlies]
1. create hourly emissions over subdomain, suitable for injecting into a CMAQ run over AQMEII-NA.
    1. compute hourly emissions over subdomain, adapting [these instructions][GFED-3.1 fractions README] to produce emissions with units=`molN2O/s`
    1. create [CMAQ][CMAQ @ CMAS]-style emissions files (e.g., [this][hourly emissions for 1 Jan 2008], when `gunzip`ed) containing emissions for every hour in 2008 (the model year).
1. check extent to which mass is conserved from global input to regional output.

Currently does not provide a clean or general-purpose (much less packaged) solution! but merely shows how to do these tasks using

* [bash][bash @ wikipedia] (tested with version=3.2.25)
* [NCL][NCL @ wikipedia] (tested with version=6.1.2)
* [R][R @ wikipedia] (tested with version=3.0.0) and packages including
    * [ncdf4][]
    * [raster][]
    * [rasterVis][]

Visualizations of datasets are provided

* statistically (summary statistics of datasets are generated and presented in console)
* by plots (saved as PDF, displayed by launching the user-configured PDF viewer)

[AQMEII-NA_N2O wiki home]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/Home
[bash @ wikipedia]: http://en.wikipedia.org/wiki/Bash_%28Unix_shell%29
[R @ wikipedia]: http://en.wikipedia.org/wiki/R_%28programming_language%29
[NCL @ wikipedia]: http://en.wikipedia.org/wiki/NCAR_Command_Language
[netCDF]: http://en.wikipedia.org/wiki/NetCDF#Format_description
[GFED-3.1 overview]: http://www.globalfiredata.org/
[GFED-3.1 data]: http://www.globalfiredata.org/Data/
[GFED-3.1 N2O archive]: http://www.falw.vu/~gwerf/GFED/GFED3/emissions/GFED3.1_N2O.zip
[GFED-3.1 N2O 2008 daily fractions archive]: ftp://gfed3:dailyandhourly@zea.ess.uci.edu/GFEDv3.1/daily/2008.tar.gz
[GFED-3.1 N2O 2008 3-hourly fractions archive]: ftp://gfed3:dailyandhourly@zea.ess.uci.edu/GFEDv3.1/3-hourly/2008.tar.gz
[converted monthly GFED-3.1 netCDF]: ../../downloads/GFED-3.1_2008_N2O.nc.gz
[GFED-3.1 fractions README]: ftp://gfed3:dailyandhourly@zea.ess.uci.edu/GFEDv3.1/Readme.pdf
[CMAQ @ CMAS]: http://www.cmaq-model.org/
[gridcell areas netCDF]: ../../downloads/areas_0.5x0.5.nc
[AQMEII-NA]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o/wiki/AQMEII-NA_spatial_domain
[visualization of global monthlies]: ../../downloads/GFED-3.1_2008_N2O_monthly_emissions_20130404_1344.pdf
[visualization of AQMEII-NA monthlies]: ../../downloads/GFED-3.1_2008_N2O_monthly_emissions_regrid_20130404_1344.pdf
[hourly emissions for 1 Jan 2008]: ../../downloads/emis_mole_N2O_20080101.ncf.gz
[ncdf4]: http://cran.r-project.org/web/packages/ncdf4/
[raster]: http://cran.r-project.org/web/packages/raster/
[rasterVis]: http://cran.r-project.org/web/packages/rasterVis/

# operation

To run this code,

1. `git clone` this repo. (See commandlines on the project homepage, where you probably are now.)
1. `cd` to its working directory (where you cloned it to).
1. Setup your applications and paths.
    1. Download a copy of [these bash utilities][bash_utilities.sh] to the working directory.
    1. Open the file in in an editor! You will probably need to edit its functions `setup_paths` and `setup_apps` to make it work on your platform. Notably you will want to point it to your PDF viewer and NCL and R executables.
    1. You may also want to open the [driver (bash) script][GFED_driver.sh] in an editor and take a look. It should run Out Of The Box, but you might need to tweak something there. In the worst case, you could hardcode your paths and apps in the driver.
    1. Once you've got it working, you may want to fork it. If so, you can automate running your changes with [uber_driver.sh][] (changing that as needed, too).
1. Run the driver:
        `$ ./GFED_driver.sh`
    This will download inputs, then run ...
    1. a set of NCL scripts to consolidate multiple GFED inputs into more tractable files (as described in steps 1.2-1.4 above)
    1. [vis_regrid_vis.r][] to visualize the monthly global emissions, regrid them to AQMEII-NA, and visualize the output
    1. [make_hourlies.ncl][] to generate hourly output (e.g., [this][hourly emissions for 1 Jan 2008]) suitable for input to [CMAQ][CMAQ @ CMAS]. Note it also includes code to view output in [VERDI][VERDI homepage] for data visualization (and, more to the point, verification of one's [IOAPI][IOAPI homepage]-correctness!)
    1. [check_conservation.ncl][] to check conservation of mass from input to output. Given that the [output spatial domain][AQMEII-NA] is significantly smaller than the input (global), it merely reports the fraction of mass (as `mol`) in output vs input. Current output includes

                GFED monthly N2O (mol) over globe from ./GFED-3.1_2008_N2O_monthly_emissions.nc:
                |cells|=3.1104e+06
                |obs|  =3.1104e+06
                min    =0
                q1     =0
                med    =0
                mean   =5.443e+03
                q3     =0
                max    =4.440e+07
                sum    =1.693e+10

                GFED monthly N2O (mol) over AQMEII-NA from ./GFED-3.1_2008_N2O_monthly_emissions_regrid.nc:
                |cells|=1.64689e+06
                |obs|  =1.64689e+06
                min    =0
                q1     =0
                med    =0
                mean   =2.337e+03
                q3     =0
                max    =1.797e+07
                sum    =3.848e+09

                AQMEII-NA monthly N2O/global monthly N2O==2.273e-01

                [aggregating hourly emissions]
                Is N2O conserved from input to output? units=mol N2O
                (note (US land area)/(earth land area) ~= 6.15e-02)
                    input      input     output     output           
                   global        NAs  AQMEII-NA        NAs     out/in
                 1.69e+10          0   3.53e+09          0   2.09e-01

                AQMEII-NA N2O (mol) from output hourly emissions:
                |cells|=1.20552e+09
                |obs|  =1.20552e+09
                min    =0.000e+00
                q1     =0.000e+00
                med    =0.000e+00
                mean   =3.103e+00
                q3     =0.000e+00
                max    =1.079e+06
                sum    =3.530e+09

[check_conservation.ncl][] can be a long-running process, even on relatively high-performance hardware. On terrae, its first phase (aggregation of hourly emissions) can take 3-5 hr wallclock; its second phase (summarizing the aggregate) has been found to run 15-90 min wallclock.

[bash_utilities.sh]: https://bitbucket.org/epa_n2o_project_team/regrid_utils/src/HEAD/bash_utilities.sh?at=master
[uber_driver.sh]: ../../src/HEAD/uber_driver.sh?at=master
[GFED_driver.sh]: ../../src/HEAD/GFED_driver.sh?at=master
[vis_regrid_vis.r]: ../../src/HEAD/vis_regrid_vis.r?at=master
[make_hourlies.ncl]: ../../src/HEAD/make_hourlies.ncl?at=master
[check_conservation.ncl]: ../../src/HEAD/check_conservation.ncl?at=master
[VERDI homepage]: http://www.verdi-tool.org/
[IOAPI homepage]: http://www.baronams.com/products/ioapi/

# TODOs

1. Retest with newest [`regrid_utils`][regrid_utils]! Currently, [`repo_diff.sh`][regrid_utils/repo_diff.sh] shows the following local `diff`s:
    * `get_filepath_from_template.ncl`
    * `IOAPI.ncl`
    * `string.ncl`
1. Move all these TODOs to [issue tracker][GFED-3.1_global_to_AQMEII-NA issues].
1. *(major)* Investigate 8% loss of mass from monthly regrids to hourly regrids.
1. [`check_raw_fractions.ncl`][check_raw_fractions.ncl]: Check that
        ftp://gfed3:dailyandhourly@zea.ess.uci.edu/GFEDv3.1/Readme.pdf
        > Over a day at each grid cell location, the sum of the eight 3-hourly fire fractions should be equal to 1.0.
1. [`check_coord_vars.ncl`][check_coord_vars.ncl]: Should also check monotonicity of coordvars? or is that guaranteed by netCDF?
1. Handle {name, path to} VERDI in [`bash_utilities.sh`][bash_utilities.sh].
1. `*.sh`: use bash booleans à la [`N2O_integration_driver.sh`][AQMEII-NA_N2O_integration/N2O_integration_driver.sh].
1. [`regrid_utils`][regrid_utils]: Make commandline [`netCDF.stats.to.stdout.r`][netCDF.stats.to.stdout.r] take files beginning with numerals, e.g.
    * (-) `Rscript ./netCDF.stats.to.stdout.r netcdf.fp=5yravg_20111219_pure_emis.nc data.var.name=N2O # fails`
    * (+) `Rscript ./netCDF.stats.to.stdout.r netcdf.fp=yravg_20111219_pure_emis.nc data.var.name=N2O # works`
1. Create common project for `regrid_resources` à la [`regrid_utils`][regrid_utils], so I don't hafta hunt down which resource is in which project.
1. All regrids: how to nudge off/onshore as required? e.g., soil or burning emissions should never be offshore, marine emissions should never be onshore..
1. All regrid maps: add Caribbean islands (esp Bahamas! for offshore burning), Canadian provinces, Mexican states.
1. [NCL][NCL @ wikipedia]: complain to ncl-talk about "unsupported extensions," e.g., .ncf and <null/> (e.g., MCIP output).
1. [R][R @ wikipedia]: determine why `<-` assignment is occasionally required in calls to `visualize.*(...)`.
1. Fully document platform versions (e.g., linux, compilers, bash, NCL, R).
1. Test on
    * tlrPanP5 (which now has R package=ncdf4, but readAsciiTable of input .txt's is very slow compared to terrae)
    * HPCC (once problem with ncdf4 on amad1 is debugged: in process with JOB and KMF)

[GFED-3.1_global_to_AQMEII-NA issues]: ../../issues
[regrid_utils]: https://bitbucket.org/epa_n2o_project_team/regrid_utils
[netCDF.stats.to.stdout.r]: https://bitbucket.org/epa_n2o_project_team/regrid_utils/src/HEAD/netCDF.stats.to.stdout.r?at=master
[check_raw_fractions.ncl]: ../../src/HEAD/check_raw_fractions.ncl?at=master
[check_coord_vars.ncl]: ../../src/HEAD/check_coord_vars.ncl?at=master
[regrid_utils/repo_diff.sh]: https://bitbucket.org/epa_n2o_project_team/regrid_utils/src/HEAD/repo_diff.sh?at=master
[AQMEII-NA_N2O_integration/N2O_integration_driver.sh]: https://bitbucket.org/epa_n2o_project_team/aqmeii-na_n2o_integration/src/HEAD/N2O_integration_driver.sh?at=master
