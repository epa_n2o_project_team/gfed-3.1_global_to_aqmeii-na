;!/usr/bin/env ncl ; requires version >= ???
;;; ----------------------------------------------------------------------
;;; Copyright 2013, 2014, 2015, 2016 Tom Roche <Tom_Roche@pobox.com>

;;; This program is free software: you can redistribute it and/or modify it provided that you do so as follows:

;;; * under the terms of the GNU Affero General Public License <https://www.gnu.org/licenses/agpl.html> as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

;;; * preserving attribution of this author in the redistributed and/or modified material. You may do so in any reasonable manner, but not in any way that suggests this author endorses you or your use.

;;; This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
;;; ----------------------------------------------------------------------


; Convert the {raw, as downloaded from GFED} netCDF 3-hour fraction files to a single netCDF file with 12 TSTEP,
; for ease of regridding as a single RasterBrick
; NOTE: very similar to convert_dailies.ncl, convert_monthlies.ncl. TODO: refactor

;----------------------------------------------------------------------
; libraries
;----------------------------------------------------------------------

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"    ; all built-ins?
;load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl" ; for stat_dispersion

load "$FILEPATH_FUNCS_FP"  ; for get_monthly_fp

;----------------------------------------------------------------------
; functions
;----------------------------------------------------------------------
  
;----------------------------------------------------------------------
; code
;----------------------------------------------------------------------

begin ; skip if copy/paste-ing to console

;----------------------------------------------------------------------
; constants
;----------------------------------------------------------------------

  ;;; inventory constants
  fracs_readme_uri = getenv("GFED_FRACS_README_URI")
  template_string = getenv("TEMPLATE_STRING")

  ;;; model constants
  this_fn = getenv("CONV_3HOURLIES_FN") ; for debugging
  model_year = stringtoint(getenv("MODEL_YEAR"))
  ; since we're dealing with numerical math:
  zero_max = stringtofloat(getenv("ZERO_MAX"))
  one_min = stringtofloat(getenv("ONE_MIN"))

  ;;; 3-hourly inputs
;   3hourly_template_string = getenv("GFED_GLOBAL_3HOURLY_FRAC_TEMPLATE_STRING")
  ; NCL variable name cannot begin with numeral
;   hour3ly_template_string = getenv("GFED_GLOBAL_3HOURLY_FRAC_TEMPLATE_STRING")
  hour3ly_fp_template = getenv("GFED_GLOBAL_3HOURLY_FRAC_FP_TEMPLATE")
  hour3ly_datavar_name = getenv("GFED_GLOBAL_3HOURLY_FRAC_DATAVAR_NAME")
  hour3ly_n_col = stringtoint(getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_N_COL"))
  hour3ly_n_row = stringtoint(getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_N_ROW"))
  
  ;;; output aggregated over year
  out_fp = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_FP")
  out_attr_creation_date = systemfunc("date") ; the canonical way
  out_attr_history = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_HISTORY")
  out_attr_source_file = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_SOURCE_FILE")
  out_attr_title = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_TITLE")

  out_datavar_name = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_DATAVAR_NAME")
  out_datavar_attr_units = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_DATAVAR_UNITS")
  out_datavar_attr_long_name = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_DATAVAR_LONG_NAME")
  ; out_datavar dims (TSTEP, LAY, ROW, COL):
  ;   TSTEP = 12 ;
  ;   LAY = 8 ;
  ;   ROW = 360 ;
  ;   COL = 720 ;
  out_n_time = stringtoint(getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_N_TSTEP"))
  out_time_arr = ispan(1,out_n_time,1) ; (/ 1, 2, ..., 12 /)
  out_n_layer = stringtoint(getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_N_LAYER"))
  out_layer_arr = ispan(0,21,3) ; hours=[0000, 0300 ..., 2100]
  out_n_row = stringtoint(getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_N_ROW"))
  ; copy out_row_arr = values(out_n_row) below (from a daily)
  out_n_col = stringtoint(getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_N_COL"))
  ; copy out_col_arr = values(out_n_col) below (from a daily)
  out_datavar_dim_sizes = \
    (/out_n_time, out_n_layer, out_n_row, out_n_col /)

;   ; start debugging---------------------------------------------------
;   printVarSummary(out_datavar_dim_sizes)
;   print("out_n_time  ='"+out_n_time+"'")
;   print("out_n_layer  ='"+out_n_layer+"'")
;   print("out_n_row    ='"+out_n_row+"'")
;   print("out_n_col    ='"+out_n_col+"'")
;   print("sizeof(grid) ='"+product( (/ out_n_row, out_n_col /) )+"'")
;   print("sizeof(brick)='"+product(out_datavar_dim_sizes)+"'")
;   ;   end debugging---------------------------------------------------

  out_dim_time_name = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_TIME_NAME")
  out_dim_time_attr_units = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_TIME_UNITS")
  out_dim_time_attr_long_name = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_TIME_LONG_NAME")

  out_dim_layer_name = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_LAYER_NAME")
  out_dim_layer_attr_long_name = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_LAYER_LONG_NAME")
  out_dim_layer_attr_units = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_LAYER_UNITS")

  out_dim_x_name = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_X_NAME")
  out_dim_x_attr_units = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_X_UNITS")
  out_dim_x_attr_long_name = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_X_LONG_NAME")

  out_dim_y_name = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_Y_NAME")
  out_dim_y_attr_units = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_Y_UNITS")
  out_dim_y_attr_long_name = getenv("GFED_GLOBAL_3HOURLY_FRAC_CONV_DIM_Y_LONG_NAME")

;----------------------------------------------------------------------
; payload
;----------------------------------------------------------------------

;----------------------------------------------------------------------
; copy output horizontal grid from any input
;----------------------------------------------------------------------

  Jan_3hourly_fp = getenv("GFED_GLOBAL_3HOURLY_FRAC_JAN_FP")
  Jan_3hourly_fh = addfile(Jan_3hourly_fp, "r")
  Jan_3hourly_lat = Jan_3hourly_fh&lat
  Jan_3hourly_lon = Jan_3hourly_fh&lon

;  ; start debugging---------------------------------------------------
;  printVarSummary(Jan_3hourly_lat)
;  printVarSummary(Jan_3hourly_lon)
;  ;   end debugging---------------------------------------------------

;----------------------------------------------------------------------
; setup annual output
;----------------------------------------------------------------------

  ;;; create output data file
  out_fh = addfile(out_fp, "c") ; use workaround if `source`ing driver

  ;; define global attributes
  out_fh_att = True ; assign file/global attributes
  out_fh_att@HISTORY = out_attr_history
  out_fh_att@YEAR = model_year
  out_fh_att@Conventions = out_attr_history
  out_fh_att@source_file = out_attr_source_file
  out_fh_att@creation_date = out_attr_creation_date
  out_fh_att@title = out_attr_title

  ;; setup file data variable (datavar) and its coordinate variables
  ;; will fill data inside input-read loop

  temp_var = new(out_datavar_dim_sizes, float)
  ; add datavar attributes
  temp_var@long_name = out_datavar_attr_long_name
  temp_var@units = out_datavar_attr_units
  ; name dimensions, provide coordinates: we want out_datavar dims
  ; (TSTEP, LAY, ROW, COL):
  ;   TSTEP = 12 ; 3hourly fractions are distributed by month ...
  ;   LAY = 8    ; ... and there are 8 3-hour periods in a day
  ;   ROW = 360  ; the usual for GFED
  ;   COL = 720  ; ditto

  ;; gotta write coordinate vars for R::raster::brick to properly setup?
  temp_var!0 = out_dim_time_name
  temp_var&$out_dim_time_name$ = out_time_arr
  ; write coordinate attrs here (not to the filehandle)
  temp_var&$out_dim_time_name$@units = out_dim_time_attr_units
  temp_var&$out_dim_time_name$@long_name = out_dim_time_attr_long_name

  ; do I need a coord var=LAY?
  temp_var!1 = out_dim_layer_name
  temp_var&$out_dim_layer_name$ = out_layer_arr
  temp_var&$out_dim_layer_name$@long_name = out_dim_layer_attr_long_name
  temp_var&$out_dim_layer_name$@units = out_dim_layer_attr_units

  ; just copy the dimension?
  temp_var!2 = out_dim_y_name
  temp_var&$out_dim_y_name$ = Jan_3hourly_lat
  ; don't need these, since I copied the dimension
;  temp_var&$out_dim_y_name$@units = out_dim_y_attr_units
;  temp_var&$out_dim_y_name$@long_name = out_dim_y_attr_long_name
  
  temp_var!3 = out_dim_x_name
  temp_var&$out_dim_x_name$ = Jan_3hourly_lon
  ; don't need these, since I copied the dimension
;  temp_var&$out_dim_x_name$@units = out_dim_x_attr_units
;  temp_var&$out_dim_x_name$@long_name = out_dim_x_attr_long_name

;   ; start debugging-----------------------------------------------------
;   printVarSummary(temp_var)
;   ;   end debugging-----------------------------------------------------

;----------------------------------------------------------------------
; iterate 3hourly inputs
;----------------------------------------------------------------------

  do i_month = 1, 12
    hour3ly_fp = get_monthly_fp(\
      hour3ly_fp_template, template_string, model_year, i_month)

;     ; start debugging-------------------------------------------------
;     ; TODO: replace with real progress control!
;     print("processing hour3ly_fp='"+hour3ly_fp+"'") 
;     ;   end debugging-------------------------------------------------

    hour3ly_fh = addfile(hour3ly_fp, "r")
    hour3ly_datavar = hour3ly_fh->$hour3ly_datavar_name$

;    ; start debugging-----------------------------------------------------
;    printVarSummary(hour3ly_datavar)
;    ;   end debugging-----------------------------------------------------

    ; Fraction_of_Emissions(time, lat, lon) ,
    ; where |time|=8 == (24 hr/day)/(3 hr)
    hour3ly_data = (/ hour3ly_datavar(:,:,:) /)

    date_index = i_month - 1 ; zero-based
    ;;; write data to output
    temp_var(date_index, :, :, :) = (/ hour3ly_data /)
    ; note: if one does instead
;             :,:) = hour3ly_data
    ; NCL will delete dimension names from temp_var!
  end do ; i_month

  ;;; finish output data file
  ;; write datavar
  ; TODO: replace with real progress control!
  print("writing data to out_fp='"+out_fp+"'") 
  out_fh->$out_datavar_name$ = temp_var

  ;; write global attributes: can (must?) do this after writing datavar
  fileattdef(out_fh, out_fh_att)

end ; convert_3hourlies.ncl
